﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    public class AccountModel : BaseModel
    {
        public string username { get; set; }
        public int type { get; set; }
        public int status { get; set; }
        public string address { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public ImageModel image { get; set; }
        public string position { get; set; }
        public string team_name { get; set; }
        public string team_uuid { get; set; }
    }

    public class AccountProfileModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public AccountModel user_info { get; set; }
        public SipModel sip_account { get; set; }
        public string position { get; set; }
        public Staff_Team team { get; set; }
        public Staff_Team department { get; set; }
        public Staff_Team branch { get; set; }
        public string uuid { get; set; }
        public int role { get; set; }
    }
    public class SipModel : BaseModel
    {

        public int exten { get; set; }
        public string password { get; set; }
        public string ami_user { get; set; }
        public string ami_password { get; set; }

    }
    public class Staff_Team
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class AccountDataModel
    {
        public AccountSearchRequest Request { get; set; }
        public List<AccountModel> Accounts { get; set; }
    }
    public class ProfileSip
    {
        public int get_sip_account { get; set; }
    }
    public class ProfilerModel
    {
        public AccountModel Account { get; set; }
        public AccountChangePasswordModel ChangePassword { get; set; }
    }

    public class AccountChangePasswordModel
    {
        [Required(ErrorMessage = "Vui lòng nhập mật khẩu cũ")]
        [DataType(DataType.Password)]
        [MaxLength(30, ErrorMessage = "Bạn nhập mật khẩu quá dài")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải quá 6 ký tự")]
        public string old_password { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu mới")]
        [DataType(DataType.Password)]
        [MaxLength(30, ErrorMessage = "Bạn nhập mật khẩu quá dài")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải quá 6 ký tự")]
        public string new_password { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập lại mật khẩu mới")]
        [DataType(DataType.Password)]
        [MaxLength(30, ErrorMessage = "Bạn nhập mật khẩu quá dài")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải quá 6 ký tự")]
        public string verify_new_password { get; set; }
    }

    public class ResetPasswordRequest
    {
        public string uuid { get; set; }
    }

    #region Request
    public class AccountSearchRequest : BaseRequest
    {
        public int? type { get; set; }
    }
    #endregion

    #region Response


    public class AccountDataResponse : AccountProfileModel
    {
        public AccountDataResponse()
        {
            items = new List<AccountModel>();
        }
        public List<AccountModel> items { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int role { get; set; }

    }

    public class MenusStringDataResponse
    {
        public MenusStringDataResponse()
        {
            items = new List<string>();
        }
        public List<string> items { get; set; }
    }
    #endregion
}
