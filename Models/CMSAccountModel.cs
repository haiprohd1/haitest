﻿using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    public class CMSAccountModel
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public double gameCoin { get; set; }
        public double gameVipLevel { get; set; }
        public int investLevel { get; set; }
        public DateTime? createAt { get; set; }
        public string createAt_format
        {
            get
            {
                if (createAt.HasValue)
                {
                    return createAt.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
        public State state { get; set; }
        public int status { get; set; }
        public Sponsor sponsor { get; set; }
    }
    public class ListAccountInvestModel
    {
        public string uuid { get; set; }
        public MyInvest invest { get; set; }
        public DateTime? timeCreated { get; set; }
        public long dayClaimed { get; set; }
        public int status { get; set; }
        public string timeCreated_format
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("yyyy/MM/dd HH:mm:ss");
                }
                return string.Empty;
            }
        }
    }
    public class MyInvest
    {
        public double price { get; set; }
        public int days { get; set; }
        public float percentPerDay { get; set; }
        public string investGroupUuid { get; set; }
        public string name { get; set; }
    }
    public class State
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<string> account { get; set; }
    }
    public class Sponsor
    {

    }
    public class ListSessionModel
    {
        public int id { get; set; }
        public string account { get; set; }
        public string ipAddress { get; set; }
        public DateTime? timeCreated { get; set; }
        public string timeCreated_format
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
    }
    public class userInvestInfo
    {
         public double totalProfit { get; set; }
         public double averageProfit { get; set; }
         public double totalAmountInvested { get; set; }
         public double totalAmountOwed { get; set; }
        public string totalProfit_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalProfit);

            }
        }     
        public string averageProfit_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", averageProfit);

            }
        }   
        public string totalAmountInvested_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalAmountInvested);

            }
        }     
        public string totalAmountOwed_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalAmountOwed);

            }
        }
    }       
    public class userInfo
    {
         public double averageOfTransaction { get; set; }
         public double totalGameWinLose { get; set; }
         public double averageTotalWinLose { get; set; }
        public string averageTotalWinLose_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", averageTotalWinLose);

            }
        }

    }   
    public class systemInfo
    {
        public double commissionProfit { get; set; }
        public string commissionProfit_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", commissionProfit);

            }
        }
        public double averageCommissionProfit { get; set; }
        public string averageCommissionProfit_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", averageCommissionProfit);

            }
        }
        public long memberCount { get; set; }
        public long memberInvestingCount { get; set; }
        public long f1Count { get; set; }
        public long f1InvestingCount { get; set; }
        public long f1GamingCount { get; set; }
        public double totalAmountInvest { get; set; }
        public double memberGamingCount { get; set; }
        public double totalGameBranchProfit { get; set; }
        public double totalTradeAmount { get; set; }
        public string totalAmountInvest_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalAmountInvest);

            }
        }

    }
    public class DetailModel
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string gameCoin { get; set; }
        public string gameVipLevel { get; set; }
        public string investLevel { get; set; }
        public DateTime? createAt { get; set; }
        public State state { get; set; }
        public int status { get; set; }
        public string tronWalletAddress { get; set; }
        public double totalInvestBranchProfit { get; set; }
        public Sponsor sponsor { get; set; }
        public string createAt_format
        {
            get
            {
                if (createAt.HasValue)
                {
                    return createAt.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
     
    }
    public class Last_10_Deposit
    {
        public string uuid { get; set; }
        public double amount { get; set; }
        public int status { get; set; }
        public string statusName { get; set; }
        public DateTime? timeCreated { get; set; }
        public DateTime? time_verified { get; set; }
        public string timeCreated_format
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
        public string time_verified_format
        {
            get
            {
                if (time_verified.HasValue)
                {
                    return time_verified.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
    }
    public class Last_10_Transaction
    {
        public string productName { get; set; }
        public string ActionName { get; set; }
        public double gameCoinChanged { get; set; }
        public double gameCoinBalance { get; set; }
        public DateTime? timeCreated { get; set; }
        public string timeCreated_format
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
    }
    public class Status
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    #region Request
    public class ListCMSAccountRequest : BaseRequest
    {
        public string roleUuid { get; set; }

    }
    public class ListSessionRequest : BaseRequest
    {
        public DateTime? timeFrom { get; set; }
        public DateTime? timeTo { get; set; }

    }
    public class DetailCMSAccountRequest
    {
        public string accountUuid { get; set; }
        public string tokenStr { get; set; }

    }

    public class UpdateStatusRequest
    {
        public string accountUuid { get; set; }
        public int newStatus { get; set; }
        public string tokenStr { get; set; }
    }
    public class ListAccountInvestRequest : BaseRequest
    {
        public string accountUuid { get; set; }

    }
    #endregion

    #region Response
    public class CMSAccountDataResponse
    {
        public CMSAccountDataResponse()
        {
            items = new List<CMSAccountModel>();
            pagination = new PaginationResponse();
        }
        public List<CMSAccountModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public DetailModel detail { get; set; }
        public userInfo userInfo { get; set; }
        public userInvestInfo userInvestInfo { get; set; }
        public systemInfo systemInfo { get; set; }
        public List<Last_10_Deposit> last_10_deposit { get; set; }
        public List<Last_10_Deposit> last_10_withdraw { get; set; }
        public List<Last_10_Transaction> last_10_transaction { get; set; }
    }
    public class ListSessionDataResponse
    {
        public ListSessionDataResponse()
        {
            items = new List<ListSessionModel>();
            pagination = new PaginationResponse();
        }
        public List<ListSessionModel> items { get; set; }
        public PaginationResponse pagination { get; set; }

    }        
    public class ListAccountInvestDataResponse
    {
        public ListAccountInvestDataResponse()
        {
            items = new List<ListAccountInvestModel>();
            pagination = new PaginationResponse();
        }
        public List<ListAccountInvestModel> items { get; set; }
        public PaginationResponse pagination { get; set; }

    }
    #endregion 
}
