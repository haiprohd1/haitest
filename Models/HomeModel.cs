﻿using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    public class HomeModel 
    {
    }
    public class DashboardModel
    {
        public DateTime? time { get; set; }
        public int count { get; set; }
        public int countCharge { get; set; }
        public int countWithdraw { get; set; }
        public string time_format
        {
            get
            {
                if (time.HasValue)
                {
                    return time.Value.ToString("MM/dd");
                }
                return string.Empty;
            }
        }
        public string time_hour_format
        {
            get
            {
                if (time.HasValue)
                {
                    return time.Value.ToString("HH:mm");
                }
                return string.Empty;
            }
        }
    }
    public class depositWithdrawChart
    {
        public DateTime time { get; set; }
        
        public string time_format
        {
            get
            {
                if (time.Hour == 0 && time.Minute == 0 && time.Second == 0)
                {
                    return time.ToString("yyyy/MM/dd");
                }
                else
                {
                    return time.ToString("yyyy/MM/dd HH:mm");
                }
                return string.Empty;
            }
        }
        public float value { get; set; }
        public float decimalValue { get; set; }
        public float countCharge { get; set; }
        public float countWithdraw { get; set; }
    }
    #region Request
    public class DashboardRequest : BaseRequest
    {
        public string tokenStr { get; set; }
        public int type { get; set; }
    }

    #endregion
    #region Response
    public class DashboardDataResponse
    {
        public List<DashboardModel> dauList { get; set; }
        public List<DashboardModel> druList { get; set; }
        public List<DashboardModel> ccuMonth { get; set; }
        public List<DashboardModel> ccuHour { get; set; }
        public List<DashboardModel> transactions { get; set; }
        public float totalRevenue { get; set; }
        public string totalRevenue_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalRevenue);

            }
        }
        public float totalTrasaction { get; set; }
        public string totalTrasaction_string
        {
            get
            {

                return string.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}", totalTrasaction);

            }
        }
        public int totalMatch { get; set; }
        public int totalBet { get; set; }
        public double totalTradingVolume { get; set; }
        public double totalUserTrading { get; set; }
        public double totalMatchBetting { get; set; }
        public double totalMatchOnAir { get; set; }

    }
    public class DashboardListDataResponse
    {
        public DashboardListDataResponse()
        {
            items = new List<depositWithdrawChart>();
            pagination = new PaginationResponse();
        }
        public List<depositWithdrawChart> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string time { get; set; }
        public float value { get; set; }
        public float decimalValue { get; set; }
    }
        #endregion
    }
