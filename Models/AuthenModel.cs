﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    public class AuthenModel
    {
    }
    public class LoginModel
    {

        public string userStr { get; set; }                         

        //[DataType(DataType.Password)]
        //[Required(ErrorMessage = "Bạn chưa nhập mật khẩu")]
        //[MinLength(6, ErrorMessage = "Mật khẩu phải từ 6 ký tự trở lên")]
        //[RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9]).*$", ErrorMessage = "Mật khẩu gồm cả số và chữ")]
        public string passStr { get; set; }
        public string ReturnUrl { get; set; }
        public string language { get; set; }
    }

    public class token
    {
        public string key { get; set; }
        public DateTime? timeStart { get; set; }
        public DateTime? timeEnd { get; set; }
        public DateTime? timeDestroy { get; set; }
    }
    public class User
    {
        public string uuid { get; set; }
        public string username { get; set; }
        public int role_id { get; set; }
    }
    #region Request
    public class LoginRequest
    {
        public string osKey { get; set; } = "WebAdmin";
        public bool revokeAll { get; set; } = false;
        public string transferKeyStr { get; set; }
        public string userStr { get; set; }
        public string passStr { get; set; }
        
    }
    public class ChangePass
    {
        public string passOldStr { get; set; }
        public string passNewStr { get; set; }
        public string tokenStr { get; set; }
    }
    public class LogoutRequest
    {
        public string transferKeyStr { get; set; }
        public string tokenStr { get; set; }
        public bool revokeAll { get; set; } = false;
    }

    public class RefreshTokenRequest
    {
        public string refresh_token { get; set; }
    }
    public class StaffDetail
    {
        public string uuid { get; set; }
        public DateTime? createAt { get; set; }
        public DateTime? updateAt { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public AccountLoginModel account { get; set; }
        public CompanyTreeLoginModel companyTree { get; set; }
        public string avatarPath { get; set; }
    }
    public class AccountLoginModel
    {
        public string uuid { get; set; }
        public DateTime? createAt { get; set; }
        public string username { get; set; }
        public RoleLoginModel role { get; set; }

    }
    public class RoleLoginModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class CompanyTreeLoginModel
    {
        public string osType { get; set; }
        public RootLoginModel root { get; set; }
    }
    public class RootLoginModel
    {
        public string description { get; set; }
    }

    #endregion

    #region Response
    public class LoginDataResponse
    {
        public token token { get; set; }
        public DateTime? tokenTimeEnd { get; set; }
        public User user { get; set; }
    }
    #endregion 
}
