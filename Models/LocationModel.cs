﻿using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    #region model
    public class LocationModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    #endregion
    #region request
    public class LocationRequest : BaseRequest
    {
        public int? province_id { get; set; }
        public int? town_id { get; set; }
    }
    #endregion
    #region 
    public class LocationDataResponse
    {
        public LocationDataResponse()
        {
            items = new List<LocationModel>();
            pagination = new PaginationResponse();
        }
        public List<LocationModel> items { get; set; }
        public PaginationResponse pagination { get; set; }

    }
    #endregion
}


