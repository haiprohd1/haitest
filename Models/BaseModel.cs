﻿using ProjectAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Models
{
    public class BaseModel
    {
        public string uuid { get; set; }
        public int? id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public DateTime? time_created { get; set; }
        public DateTime? time_last_update { get; set; }
        public State state { get; set; }
        public Type type { get; set; }

        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string time_last_update_format
        {
            get
            {
                if (time_last_update.HasValue)
                {
                    return time_last_update.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public class State
        {
            public int id { get; set; }
            public string name { get; set; }
            public string color { get; set; }

        }
        public class Status
        {
            public int status { get; set; }
            public string name { get; set; }
            public string color { get; set; }

        }
        public class Type
        {
            public int id { get; set; }
            public string name { get; set; }
            public string color { get; set; }
        }
        public class Macth
        {
            public string teamHome { get; set; }
            public string iconTeamHome { get; set; }
            public string teamAway { get; set; }
            public string iconTeamAway { get; set; }
            public string name { get; set; }
            public DateTime? matchTime { get; set; }
            public string matchTime_format { get { return Helpers.DateTime2String(matchTime); } }
        }

        public class Bet
        {
            public int betType { get; set; }
            public int homeScore { get; set; }
            public int awayScore { get; set; }
            public float interest { get; set; }
            public string interest_format { get { return Helpers.Float2USMoney(interest); } }
        }
    }
 
}
