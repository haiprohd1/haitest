﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface IImageService
    {
        ResponseData<ImageDataResponse> Add(RequestHeader header, ImageAddRequest request);
        ResponseData<ImageDataResponse> Add(RequestHeader header, string input);
        ResponseImageData PostFile(RequestHeader header, List<FileUpload> input);
    }
}
