﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface IAuthenService
    {
        ResponseData<LoginDataResponse> RefreshToken(RequestHeader header, RefreshTokenRequest request);
        ResponseData<LoginDataResponse> Login(RequestHeader header, LoginRequest request);
        ResponseData<LoginDataResponse> Logout(RequestHeader header,LogoutRequest request);
        ResponseData<LoginDataResponse> ChangePassword(RequestHeader header, ChangePass request);

    }
}
