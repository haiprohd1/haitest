﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using ProjectAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface IAccountService
    {
        ResponseData<AccountDataResponse> ChangePassword(RequestHeader header, object request);
        ResponseData<AccountDataResponse> ResetPassword(RequestHeader header, object request);
        //ResponseData<AccountDataResponse> ResetPassword(RequestHeader header, ResetPasswordRequest request);
        ResponseData<AccountDataResponse> Gets(RequestHeader header, AccountSearchRequest request);
        ResponseData<AccountDataResponse> Profile(RequestHeader header);
        ResponseData<AccountDataResponse> ProfileSip(RequestHeader header, ProfileSip request);
        ResponseData<MenusStringDataResponse> Menus(RequestHeader header);
    }
}
