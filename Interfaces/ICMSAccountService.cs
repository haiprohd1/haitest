﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface ICMSAccountService
    {
        ResponseData<CMSAccountDataResponse> Gets(RequestHeader header, ListCMSAccountRequest request);
        ResponseData<CMSAccountDataResponse> UpdateStatus(RequestHeader header, UpdateStatusRequest request);
        ResponseData<ListAccountInvestDataResponse> ListAccountInvest(RequestHeader header, ListAccountInvestRequest request);
        ResponseData<CMSAccountDataResponse> InfoGame(RequestHeader header, ListAccountInvestRequest request);
        ResponseData<CMSAccountDataResponse> Detail(RequestHeader header, DetailCMSAccountRequest request);
        ResponseData<ListSessionDataResponse> GetsListSession(RequestHeader header, ListSessionRequest request);
    }
}
