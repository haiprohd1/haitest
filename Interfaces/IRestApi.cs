﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface IRestApi<T>
    {
        T CallApi(RequestHeader header, string input);
        T PostFile(RequestHeader header, List<FileUpload> input);
    }
}
