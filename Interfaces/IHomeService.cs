﻿using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Interfaces
{
    public interface IHomeService
    {
        ResponseData<DashboardDataResponse> Dashboard(RequestHeader header, DashboardRequest request);
        ResponseData<DashboardListDataResponse> WithDraw(RequestHeader header, DashboardRequest request);
        ResponseDataList<DashboardListDataResponse> Statical(RequestHeader header, DashboardRequest request);
    }
}
