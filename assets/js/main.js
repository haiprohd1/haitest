// import '../sass/style.scss';
toastr.options = {
    "closeButton": true,
    "positionClass": "toast-top-right",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
window.$ = window.jQuery = require('jquery');
window.PerfectScrollbar = require('perfect-scrollbar').default;
window.Cookies = require('js-cookie');
window.swal = require('sweetalert');
window.autosize = require('autosize');
//window.Highcharts = require('highcharts');
window.imageDomain = 'https://Project-sandbox.m2w.vn:1285/';
window.domain = 'https://pmbhbt.com:1244';
window.moment = require('moment');

import 'moment/locale/vi';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/alert';
import 'bootstrap/js/dist/carousel';
import 'bootstrap/js/dist/button';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/popover';
import 'bootstrap/js/dist/tab';
import 'bootstrap/js/dist/toast';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap-select/dist/js/bootstrap-select';
import 'block-ui/jquery.blockUI';
import 'jquery-validation/dist/jquery.validate';
import 'bootstrap-daterangepicker/daterangepicker.js';
import 'bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js';
import 'jstree';
import 'jquery-serializejson';
//import 'datatables.net';
import 'datatables-treegrid/dataTables.treeGrid';


import './vendors/bootstrap-notify';
window.KTAppOptions = {};
window.KTUtil1 = require("./core/util");
window.KTApp1 = require("./core/app");
window.KTWizard1 = require("./core/base/datatable/core.datatable");
window.KTDrawer1 = require("./core/base/drawer");
import './vendors/jquery-validation.init';
import './vendors/bootstrap-datepicker.init';
import './vendors/bootstrap-timepicker.init';
import './vendors/dropzone.init';
import '../vendors/chart.js/dist/chart';

//page
require('./pages/AccountSettings');
require('./pages/report');
require('./pages/TransactionHistory');
require('./pages/authen');

require('./pages/home');



