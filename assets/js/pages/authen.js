﻿





class AuthenForgotPass {
    constructor(selector) {
        this.selector = selector;

        if ($(`${this.selector}`).length) {
            this.init();
        }
    }

    init() {
        this.initControl();
        this.eventListener();
        this.initValidation();
    }

    eventListener() {
        //$(document).off(`click`, `#authen_forgot_form`).on(`click`, `#authen_forgot_form`, e => {
        //    var span = document.createElement("span");
        //    span.innerHTML = "Chúc mừng bạn đã đăng ký tài khoản thành công trên PTS. Bạn vui lòng vào liên kết trong mail để xác nhận tài khoản này là của bạn.";

        //    KTApp.swal({
        //        title: 'Đăng ký thành công!',
        //        content: span,
        //        className: 'ver_black',
        //        //customClass: {
        //        //    container: 'ver_black',
        //        //},
        //        buttons: {
        //            cancel: {
        //                text: 'Đóng lại',
        //                value: null,
        //                visible: true,
        //                className: 'btn btn-black ver_2',
        //                closeModal: true,
        //            },
        //            confirm: {
        //                text: 'Truy cập Email',
        //                value: true,
        //                visible: true,
        //                className: 'btn btn-success ver_3',
        //                closeModal: true,
        //            }
        //        }
        //    }).then((confirm) => {
        //        if (confirm) {

        //        } else {

        //        }
        //    });
        //});
    }
    initControl() {
        //if ($(`.showHidePassInput`).length) {
        //    this.func.showHidePassInput(`.showHidePassInput`);
        //}
    }
    initValidation() {
        if ($(`#authen_forgot_form`).length) {
            $(`#authen_forgot_form`).validate({
                rules: {
                    email: { required: true, email: true }
                },
                messages: {
                    email: { required: "Vui lòng nhập email!" }
                },
                submitHandler: function (form) {
                    KTApp1.block($(form).parent().parent());
                    var data = $(`#authen_forgot_form`).serializeJSON() || {};

                    Swal.fire({
                        //title: 'Do you want to save the changes?',
                        //icon: '/img/authen/Group (2).png',
                        imageUrl: '/img/authen/Group (2).png',
                        width: 450,
                        html: `Chúng tôi sẽ gửi cho bạn một email với một liên kết đặt lại mật khẩu tới địa chỉ ${data.email}. Bạn vui lòng check hộp thư trong Email để đặt lại mật khẩu. Xin cảm ơn!`,
                        //showDenyButton: true,
                        confirmButtonText: 'Truy cập vào Email',
                        //denyButtonText: `Close`,
                        showCloseButton: true,
                        customClass: {
                            container: 'ver_2',
                            confirmButton: 'btn btn-success ver_2',
                        },
                    }).then((result) => {
                        if (result.isConfirmed) {
                            KTApp.unblock($(form).parent().parent());
                        } else {
                            KTApp.unblock($(form).parent().parent());
                        }
                    });
                },
            });
        }
    }
}
class AuthenChangePassword {
    constructor(selector) {
        this.selector = selector;
        this.func = require(`./func.js`);

        if ($(`${this.selector}`).length) {
            this.init();
        }
    }

    init() {
        this.initControl();
        this.eventListener();
        this.initValidation();
    }

    eventListener() {
        //$(document).off(`click`, `#authen_forgot_form`).on(`click`, `#authen_forgot_form`, e => {
        //    var span = document.createElement("span");
        //    span.innerHTML = "Chúc mừng bạn đã đăng ký tài khoản thành công trên PTS. Bạn vui lòng vào liên kết trong mail để xác nhận tài khoản này là của bạn.";

        //    KTApp.swal({
        //        title: 'Đăng ký thành công!',
        //        content: span,
        //        className: 'ver_black',
        //        //customClass: {
        //        //    container: 'ver_black',
        //        //},
        //        buttons: {
        //            cancel: {
        //                text: 'Đóng lại',
        //                value: null,
        //                visible: true,
        //                className: 'btn btn-black ver_2',
        //                closeModal: true,
        //            },
        //            confirm: {
        //                text: 'Truy cập Email',
        //                value: true,
        //                visible: true,
        //                className: 'btn btn-success ver_3',
        //                closeModal: true,
        //            }
        //        }
        //    }).then((confirm) => {
        //        if (confirm) {

        //        } else {

        //        }
        //    });
        //});
    }
    initControl() {
        if ($(`.showHidePassInput`).length) {
            this.func.showHidePassInput(`.showHidePassInput`);
        }
    }
    initValidation() {
        //if ($(`#authen_forgot_form`).length) {
        //    $(`#authen_forgot_form`).validate({
        //        rules: {
        //            email: { required: true, email: true }
        //        },
        //        messages: {
        //            email: { required: "Vui lòng nhập email!" }
        //        },
        //        submitHandler: function (form) {
        //            KTApp.block($(form).parent().parent());
        //            var data = $(`#authen_forgot_form`).serializeJSON() || {};

        //            Swal.fire({
        //                //title: 'Do you want to save the changes?',
        //                //icon: '/img/authen/Group (2).png',
        //                imageUrl: '/img/authen/Group (2).png',
        //                width: 450,
        //                html: `Chúng tôi sẽ gửi cho bạn một email với một liên kết đặt lại mật khẩu tới địa chỉ ${data.email}. Bạn vui lòng check hộp thư trong Email để đặt lại mật khẩu. Xin cảm ơn!`,
        //                //showDenyButton: true,
        //                confirmButtonText: 'Truy cập vào Email',
        //                //denyButtonText: `Close`,
        //                showCloseButton: true,
        //                customClass: {
        //                    container: 'ver_2',
        //                    confirmButton: 'btn btn-success ver_2',
        //                },
        //            }).then((result) => {
        //                if (result.isConfirmed) {
        //                    KTApp.unblock($(form).parent().parent());
        //                } else {
        //                    KTApp.unblock($(form).parent().parent());
        //                }
        //            })
        //        },
        //    });
        //}
    }
}
class AuthenLogin {
    constructor(selector) {
        this.selector = selector;
        this.func = require(`./func.js`);

        if ($(`${this.selector}`).length) {
            this.init();
        }
    }

    init() {
        this.initControl();
        this.eventListener();
        this.initValidation();
    }

    eventListener() {
        var valueLanguage = $("#language-picker-select").val();
        $("#language").val(valueLanguage);
        $("#authen_login_rememberPass").on('change', function () {
            var username = $("#userStr").val();
            var password = $("#passStr").val();
            var checked = $("#authen_login_rememberPass").is(':checked');
            if (checked) {
                localStorage.setItem("userStr", username);
                localStorage.setItem("passStr", password);
            } else {
                localStorage.setItem("userStr", "");
                localStorage.setItem("passStr", "");
            }
        });
        if (localStorage.getItem("userStr") != "" &&  localStorage.getItem("passStr") != "") {
            debugger
            $("#authen_login_rememberPass").prop('checked', true);
            $("#userStr").val(localStorage.getItem("userStr"));
            $("#passStr").val(localStorage.getItem("passStr"));
        }
    }
    initControl() {
        if ($(`.showHidePassInput`).length) {
            this.func.showHidePassInput(`.showHidePassInput`);
        }
    }
    initValidation() {
        //if ($(`#authen_forgot_form`).length) {
        //    $(`#authen_forgot_form`).validate({
        //        rules: {
        //            email: { required: true, email: true }
        //        },
        //        messages: {
        //            email: { required: "Vui lòng nhập email!" }
        //        },
        //        submitHandler: function (form) {
        //            KTApp.block($(form).parent().parent());
        //            var data = $(`#authen_forgot_form`).serializeJSON() || {};

        //            Swal.fire({
        //                //title: 'Do you want to save the changes?',
        //                //icon: '/img/authen/Group (2).png',
        //                imageUrl: '/img/authen/Group (2).png',
        //                width: 450,
        //                html: `Chúng tôi sẽ gửi cho bạn một email với một liên kết đặt lại mật khẩu tới địa chỉ ${data.email}. Bạn vui lòng check hộp thư trong Email để đặt lại mật khẩu. Xin cảm ơn!`,
        //                //showDenyButton: true,
        //                confirmButtonText: 'Truy cập vào Email',
        //                //denyButtonText: `Close`,
        //                showCloseButton: true,
        //                customClass: {
        //                    container: 'ver_2',
        //                    confirmButton: 'btn btn-success ver_2',
        //                },
        //            }).then((result) => {
        //                if (result.isConfirmed) {
        //                    KTApp.unblock($(form).parent().parent());
        //                } else {
        //                    KTApp.unblock($(form).parent().parent());
        //                }
        //            })
        //        },
        //    });
        //}
    }
}

$(document).ready(function () {
    let authenForgotPass = new AuthenForgotPass(`.authen_forgotPassword`);
    let authenChangePassword = new AuthenChangePassword(`.authen_changePassword`);
    let authenLogin = new AuthenLogin(`.Login`);
});
