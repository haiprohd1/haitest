﻿"use strict";

var LKCReport = function () {
    var dataTableInstance;
    var dataTableInstanceDetail;
    var language = $("#language-picker-select").val();
    var startlocal = moment().subtract(29, 'days');
    var endlocal = moment();
    var flagcb = 0;
    var LIMIT = 20;
    var START_PAGE = 0;
    var drawer = null;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/TransactionHistory/test',
                    },
                    response: {

                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            pagination.total = pagination.totalCount;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }

                        },

                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, actionId, productId } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val().trim()),
                            page: pagination.page,
                            actionId: actionId || $("#js-filter-action").val(),
                            productId: productId || $("#js-filter-product").val(),
                            timeFrom: startlocal.format("YYYY/MM/DD"),
                            timeTo: endlocal.format("YYYY/MM/DD")
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'User',
                    title: language == "" ? 'Người dùng' : 'User',
                    autoHide: false,
                    template: function (data) {
                        var output = `
                            <a href="/report/detail"><span ><span>Camvan1511</span></span></a>
                        `;
                        return output;
                    },
                    width: 80

                },
                {
                    field: 'Transaction',
                    title: language == "" ? 'Tổng giao dịch' : 'Total transaction',
                    autoHide: false,
                    template: function (data) {

                        var output = `
                            <a>TLP -> LKP</a>
                        `;
                        return output;
                    },
                    width: 80
                },
                {
                    field: 'TLP nội bộ',
                    title: language == "" ? 'TLP nội bộ' : 'Internal TLP',
                    autoHide: false,
                    width: 90,
                    template: function (data) {

                        var output = `
                            <a>TLP nội bộ</a>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Convert',
                    title: language == "" ? 'Quy đổi TLP' : 'Convert TLP',
                    autoHide: false,
                    width: 80,
                    template: function (data) {
                        var output = `
                                <span>20</span>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Nạp LKP',
                    title: language == "" ? 'Nạp LKP' : 'Load LKP',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>Thành công</span>
                         `;
                        return output;
                    },

                },
                {
                    field: 'Tổng Point',
                    title: language == "" ? 'Tổng Point' : 'Total Points',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: 'Tổng Coin',
                    title: language == "" ? 'Tổng Coin' : 'Total Coin',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: 'Giao dịch lần cuối',
                    title: language == "" ? 'Giao dịch lần cuối' : 'Last transaction',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: '',
                    title: '',
                    autoHide: false,
                    width: 10,
                    template: function (data) {
                        var output = `
                                <span class="remove-report"><i class="ri-more-2-fill" style="font-size:24px;"></i></span>
                         `;

                        return output;
                    },

                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });

    };
    var initTableDetail = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstanceDetail = $('.kt-datatable-detail').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/TransactionHistory/test',
                    },
                    response: {

                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            pagination.total = pagination.totalCount;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }

                        },

                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, actionId, productId } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val().trim()),
                            page: pagination.page,
                            actionId: actionId || $("#js-filter-action").val(),
                            productId: productId || $("#js-filter-product").val(),
                            timeFrom: startlocal.format("YYYY/MM/DD"),
                            timeTo: endlocal.format("YYYY/MM/DD")
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'Transaction',
                    title: language == "" ? 'Giao dịch' : 'Transaction',
                    autoHide: false,
                    template: function (data) {

                        var output = `
                            <a>TLP -> LKP</a>
                        `;
                        return output;
                    },
                    width: 80
                },
                {
                    field: 'form',
                    title: language == "" ? 'Hình thức' : 'Form',
                    autoHide: false,
                    width: 90,
                    template: function (data) {

                        var output = `
                            <a>TLP nội bộ</a>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Amount',
                    title: language == "" ? 'Số lượng' : 'Amount',
                    autoHide: false,
                    width: 80,
                    template: function (data) {
                        var output = `
                                <span>20</span>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Status',
                    title: language == "" ? 'Trạng thái' : 'Status',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>Thành công</span>
                         `;
                        return output;
                    },

                },
                {
                    field: 'Created Time',
                    title: language == "" ? 'Thời gian tạo' : 'Created Time',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: 'Processing Time',
                    title: language == "" ? 'Thời gian xử lý' : 'Processing Time',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });

    };
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
  
   

    
    var initEventListeners = function () {
        $(document)
            .off('click', ".remove-report")
            .on('click', ".remove-report", function () {
                initRemove();
        })
    }
    var initSearch = function () {
      
       
    }
  
    var initRemove = function (id) {
        Swal.fire({
            title: 'Xóa báo cáo thống kế',
            text: "Xác nhận xóa báo cáo thống kê này.",
            showCancelButton: true,
            confirmButtonText: 'Đồng ý',
            cancelButtonText: 'Hủy bỏ',
            customClass: {
                popup: 'customRemove',
                title: 'titleRemove',
                closeButton: 'your-close-button-class',
                content: 'contentRemove',
                confirmButton: 'btnConfirm',
                cancelButton: 'btnCancel',
              
            }
            
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
               a
            } 
        })
    }
    return {
        // Public functions
        init: function () {
            initTable();
            initTableDetail();
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.report').length) {
        LKCReport.init();
    }
});
