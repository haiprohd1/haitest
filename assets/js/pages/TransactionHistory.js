﻿var HistoryTransaction = function () {
    var dataTableInstance;
    var LIMIT = 20;
    var START_PAGE = 0;
    var startlocal = moment().subtract(29, 'days');
    var endlocal = moment();
    var language = $("#language-picker-select").val();
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/TransactionHistory/test',
                    },
                    response: {

                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            pagination.total = pagination.totalCount;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }

                        },

                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, actionId, productId } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val().trim()),
                            page: pagination.page,
                            actionId: actionId || $("#js-filter-action").val(),
                            productId: productId || $("#js-filter-product").val(),
                            timeFrom: startlocal.format("YYYY/MM/DD"),
                            timeTo: endlocal.format("YYYY/MM/DD")
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
           
            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'User',
                    title: language == "" ? 'Người dùng' : 'User',
                    autoHide: false,
                    template: function (data) {
                        var output = `
                            <a><span ><span>Camvan1511</span></span></a>
                        `;
                        return output;
                    },
                    width: 80

                },
                {
                    field: 'Transaction',
                    title: language == "" ? 'Giao dịch' : 'Transaction',
                    autoHide: false,
                    template: function (data) {

                        var output = `
                            <a>TLP -> LKP</a>
                        `;
                        return output;
                    },
                    width: 80
                },
                {
                    field: 'form',
                    title: language == "" ? 'Hình thức' : 'Form',
                    autoHide: false,
                    width: 90,
                    template: function (data) {

                        var output = `
                            <a>TLP nội bộ</a>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Amount',
                    title: language == "" ? 'Số lượng' : 'Amount',
                    autoHide: false,
                    width: 80,
                    template: function (data) {
                        var output = `
                                <span>20</span>
                        `;
                        return output;
                    },
                },
                {
                    field: 'Status',
                    title: language == "" ? 'Trạng thái' : 'Status',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>Thành công</span>
                         `;
                        return output;
                    },

                },
                {
                    field: 'Created Time',
                    title: language == "" ? 'Thời gian tạo' : 'Created Time',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: 'Processing Time',
                    title: language == "" ? 'Thời gian xử lý' : 'Processing Time',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        var output = `
                                <span>2021-12-23  10:22:48</span>
                         `;

                        return output;
                    },

                },
                {
                    field: '',
                    title: '',
                    autoHide: false,
                    width: 10,
                    template: function (data) {
                        var output = `
                                <span class="detail-transaction"><i class="ri-add-circle-fill" style="color: #2CAE39;font-size:24px;   "></i></span>
                         `;

                        return output;
                    },

                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });

    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.detail-transaction')
            .on('click', '.detail-transaction',function () {
                initDrawerDetail();
            })
    }
    var initDrawerDetail = function (id) {
        const title = language == "" ? "Chi tiết lịch sử giao dịch": "Details of transaction history";
        drawer = new KTDrawer1({ title, id: id || 'new', className: 'pts-drawer--edit-match-bet' });
        drawer.on('ready', () => {
            $.ajax({
                url: '/TransactionHistory/Detail',
                method: 'GET',
                data: {
                    uuid: id
                }
            })
                .done(function (response) {
                    /*  const $content = drawer.setBody(response);*/
                    drawer.setBody(response)
                    $('.recepits-btn-close').off('click').on('click', function () {
                        drawer.close();
                    })
                    $('.check_number').keypress(function (evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    })
                    $("#btn-submit-edit").on('click', function () {
                        initSubmitEditMatch();
                    })
                    $('#close-bet').on('click', function () {
                        drawer.close();
                    })
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    }
    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
      
        },
    };
}();



$(document).ready(function () {
    if ($('.HistoryTransaction').length) {
        HistoryTransaction.init();
    }
});
