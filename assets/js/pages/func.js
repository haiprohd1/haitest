﻿


let CONSTANT = {
    NOTIFY_STATUS_ACTIVE: 1,
    NOTIFY_STATUS_DISABLE: 0,

    MSG_NO_DATA: "Không có dữ liệu"
}

let showHidePassInput = function (selector) {
    if ($(`${selector}`).length) {
        let $inputGroup = $(`${selector}`).parents(`.input-group`);
        $inputGroup.off(`click`, `i`).on(`click`, `i`, e => {
            let $this = $(e.currentTarget);
            let $inputGroup = $this.parents(`.input-group`);

            $this.siblings(`i`).css('display', 'flex');
            $this.css('display', `none`);

            if ($inputGroup.find(`input`).attr(`type`) == `text`) {
                $inputGroup.find(`input`).attr('type', 'password');
            } else {
                $inputGroup.find(`input`).attr('type', 'text');
            }
        });
    }
}

let initDaterangepickerSinger = function () {

}

class DaterangepickerSingle {
    constructor(selector, settings) {
        this.selector = selector;
        this.settings = settings || {};
        this.settings.autoUpdateInput = this.settings.autoUpdateInput != undefined ? this.settings.autoUpdateInput : false;
        this.settings.singleDatePicker = this.settings.singleDatePicker != undefined ? this.settings.singleDatePicker : true;
        this.settings.showDropdowns = this.settings.showDropdowns != undefined ? this.settings.showDropdowns : true;

        if ($(selector).length) {
            this.init();
        }
    }

    init() {
        $(this.selector).daterangepicker(this.settings);
        $(this.selector).off('apply.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
        });
        $(this.selector).parent().children().children(`i`).off(`click`).on(`click`, e => {
            $(e.currentTarget).parent().parent().children(`input`).click();
        });
    }
}

class Daterangepicker2Input {
    constructor(selectorStart, selectorEnd) {
        this.selectorStart = selectorStart;
        this.selectorEnd = selectorEnd;

        if ($(selectorStart).length + $(selectorEnd).length) {
            this.init();
        }
    }

    init() {
        this.initStart();
        this.initEnd();
    }

    //getStart() {
    //    return ($(this.selectorStart).data('daterangepicker') || {}).startDate || null;
    //}

    //getEnd() {
    //    return ($(this.selectorStart).data('daterangepicker') || {}).startDate || null;
    //}

    initStart(maxDate) {
        console.log("sad",maxDate);
        let that = this;
        $(this.selectorStart).daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            showDropdowns: true,
            maxDate: maxDate || false
        });
        $(this.selectorStart).off('apply.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
            that.initEnd(picker.startDate);
        });
        $(this.selectorStart).parent().children().children(`i`).off(`click`).on(`click`, e => {
            $(e.currentTarget).parent().parent().children(`input`).click();
        });
    }

    initEnd(minDate) {
        console.log(minDate);
        let that = this;
        $(this.selectorEnd).daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            showDropdowns: true,
            minDate: minDate || false
        });
        $(this.selectorEnd).off('apply.daterangepicker').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
            that.initStart(picker.startDate);
        });
        $(this.selectorEnd).parent().children().children(`i`).off(`click`).on(`click`, e => {
            $(e.currentTarget).parent().parent().children(`input`).click();
        });
    }
}

/**
 * tableCheck: {
                    'seletorTab(taget of data-bs-target)': {
                        idTable: 'selector table',
                        table: table
                    },
               }
 * @param {any} tableCheck
 * @param {any} selectorTabList
 * 
 */
let initTab = function (tableCheck, selectorTabList) {
    var renderTable = function (table) {
        if (table) {
            if ($(table.idTable).length && parseInt($(table.idTable).attr('is-render')) != 1) {
                $(table.idTable).attr('is-render', 1);
                table.table.init;
                table.table.reload();
            } else {
                try {
                    table.table.reload();
                } catch (er) {
                    console.log(er);
                }
            }
        }
    };

    let windowHash = window.location.hash;

    let hash = window.location.hash || "";
    hash = hash != `` ? hash : $(`${selectorTabList} .active[data-bs-toggle="tab"]`).attr('data-bs-target');

    // on load of the page: switch to the currently selected tab
    var currentTable2 = tableCheck[`${window.location.hash}`] || { idParent: null };
    if (currentTable2.idParent) {
        try {
            $(`${selectorTabList} [data-bs-toggle="tab"][data-bs-target="${currentTable2.idParent}"]`).tab('show');
            $(`${selectorTabList} [data-bs-toggle="tab"][href="${currentTable2.idParent}"]`).tab('show');
            $(`${selectorTabList} [data-bs-toggle="tab"][data-bs-target="${windowHash}"]`).tab('show');
            $(`${selectorTabList} [data-bs-toggle="tab"][href="${windowHash}"]`).tab('show');
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            $(`${selectorTabList} [data-bs-toggle="tab"][data-bs-target="${window.location.hash}"]`).tab('show');
            $(`${selectorTabList} [data-bs-toggle="tab"][href="${window.location.hash}"]`).tab('show');
        } catch (err) {
            console.log(err);
        }
    }

    var currentTable = tableCheck[`${hash}`];
    if (currentTable) {
        renderTable(currentTable);
    }

    // store the currently selected tab in the hash value
    $(`${selectorTabList} [data-bs-toggle="tab"]`).on("shown.bs.tab", function (e) {
        var id = $(e.currentTarget).attr("data-bs-target") || $(e.currentTarget).attr("href");
        window.location.hash = id;

        var currentTable = tableCheck[`${id}`];
        renderTable(currentTable);
    });

};
let getFromObj = function (data, link, defaultVal) {
    let links = link.split('.');
    let dataR = data;
    for (var i = 0; i < links.length; i++) {
        dataR = (dataR || dataR == 0) ? dataR[links[i]] : null;
    }
    return (dataR || dataR == 0) ? dataR : defaultVal;
};


export {
    showHidePassInput, DaterangepickerSingle, Daterangepicker2Input, initTab, CONSTANT, getFromObj
};