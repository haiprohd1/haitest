﻿var AccountSetting = function () {
   
    var initEventListeners = function () {
        $(document).off('click', '#ChangePassword').on("click",'#ChangePassword', function () {
            ChangePassword();
        })
    }
    var ChangePassword = function (id) {
        var drawer = null;
        var id = 1;
        const title = `Đổi mật khẩu`; /*'Change Password';*/
        drawer = new KTDrawer1({ title, id: id || 'new', className: 'dmms-drawer--Change-password' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/authen/changepassword',
                method: 'GET',
            })
                .done(function (response) {
                  /*  const $content = drawer.setBody(response);*/
                    const $content = drawer.setBody(response)
                    $('.recepits-btn-close').off('click').on('click', function () {
                        drawer.close();
                    })
                   
                    //initValidation();
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    }
    var initValidation = function () {
        $("#authen_forgot_form").validate({
            rules: {
                passOldStr: {
                    required: true,
                },
                passNewStr: {
                    required: true,
                },
                newConfirmPassword: {
                    required: true,
                    equalTo:"#passNewStr"
                },
               

            },
            messages: {
              
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {


            },
            submitHandler: function (form) {
                //KTApp1.block($(form))
                $('#button__submit').attr('disabled', true);
                $('#button__submit').children().append(`<span class="spinner-border spinner-border-sm align-middle ms-2 customer__spiner"></span>`)
                const data = $("#authen_forgot_form").serializeJSON() || {};
                

                $.ajax({
                    url: `/authen/ChangPassword`,
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp1.unblock($(form));
                        const successMsg = 'Success';
                        toastr.success(successMsg);
                        drawer.close();


                    })
                    .fail(function (data) {
                        $('#button__submit').attr('disabled', false);
                        $('#button__submit').children().find('.spinner-border').remove();
                        KTApp1.unblock($(form));
                        const errorMsg = data.responseJSON.error.message;
                        toastr.error(errorMsg);
                    });
                return false;
            },
        });
    
    }

    return {
        // Public functions
        init: function () {
            initEventListeners();
            //initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('#kt_header').length) {
        AccountSetting.init();
    }
});
