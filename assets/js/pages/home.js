﻿



class Home {

    constructor(selector) {
        this.selector = selector;
        this.func = require(`./func.js`);
        this.table = null;
        this.LIMIT = 20;
        this.START_PAGE = 0;
        this.chartActiveUsers;
        this.chartMoneyStatistics;
        this.chartStaticalNru;
        this.chartStaticalUac;
        this.chartStaticalTradeVolume;
        var type = 0;
        if ($(`${this.selector}`).length) {
            this.init(type);
            $('.filter-day').addClass('active');
            $('.filter-day-StaticalUac').addClass('active');
            $('.filter-day-StaticalNru').addClass('active');
            $('.filter-day-TradeVolume').addClass('active');
        }

    }

    init(type) {
        //this.initTable();
        this.initControl();
        this.initChart(type);
        this.initChartStaticalNru(type);
        this.initChartStaticalUac(type);
        this.initChartStaticalTradeVolume(type);
        this.eventHander();
    }
    eventHander() {
        var that = this;
        $(`.filter-day`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-day`).addClass('active')
            that.initChart(0, true);

        });
        $(`.filter-month`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-month`).addClass('active')
            that.initChart(2, true);

        });
        $(`.filter-week`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-week`).addClass('active')
            that.initChart(1, true);

        });
        $(`.filter-day-StaticalNru`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-day-StaticalNru`).addClass('active')
            that.initChartStaticalNru(0, true);

        });
        $(`.filter-month-StaticalNru`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-month-StaticalNru`).addClass('active')
            that.initChartStaticalNru(2, true);

        });
        $(`.filter-week-StaticalNru`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-week-StaticalNru`).addClass('active')
            that.initChartStaticalNru(1, true);

        });
        $(`.filter-day-StaticalUac`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().parent().find('.active').removeClass('active');
            $(`.filter-day-StaticalUac`).addClass('active')
            that.initChartStaticalUac(0, true);

        });
        $(`.filter-month-StaticalUac`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().parent().find('.active').removeClass('active');
            $(`.filter-month-StaticalUac`).addClass('active')
            that.initChartStaticalUac(2, true);

        });
        $(`.filter-week-StaticalUac`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().parent().find('.active').removeClass('active');
            $(`.filter-week-StaticalUac`).addClass('active')
            that.initChartStaticalUac(1, true);
        });
        $(`.filter-day-TradeVolume`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-day-TradeVolume`).addClass('active')
            that.initChartStaticalTradeVolume(0, true);

        });
        $(`.filter-month-TradeVolume`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-month-TradeVolume`).addClass('active')
            that.initChartStaticalTradeVolume(2, true);

        });
        $(`.filter-week-TradeVolume`).off('click').on('click', function () {
            var $this = $(this);
            $this.parent().find('.active').removeClass('active');
            $(`.filter-week-TradeVolume`).addClass('active')
            that.initChartStaticalTradeVolume(1, true);
        });

    }
    initTable() {
        let that = this;
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }

        this.table = $('.table_member').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/Member/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? that.LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            that.LIMIT = perpage;
                            that.START_PAGE = 0;
                            pagination.total = pagination.totalCount;
                            //Gán các thuộc tính của images{0} vào đối tượng item

                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || that.LIMIT,
                                    page: page || 1,
                                },
                            }

                        },

                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, roleUuid } = query;
                        if (that.START_PAGE === 1) {
                            pagination.page = that.START_PAGE
                        }
                        return {
                            limit: pagination.perpage || that.LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()),
                            page: pagination.page,
                            roleUuid: roleUuid
                        };
                    }
                },
                pageSize: that.LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'Username',
                    title: 'Username',
                    autoHide: false,
                    width: 110,
                    template: function (data) {
                        return `
                            <img src="/img/member/Ellipse 25.png" class="me-2" width="24" height="24"/>
                            <a class="text-link" href="/Member/Detail">${data.name || "-"}</a>
                        `;
                    },

                },
                {
                    field: 'Email',
                    title: 'Email',
                    autoHide: false,
                    width: 150,
                    template: function (data) {
                        return `<span>name@email.com</span>`;
                    },

                },
                {
                    field: 'Số dư',
                    title: 'Số dư',
                    autoHide: false,
                    width: 70,
                    template: function (data) {
                        return `<span>7854502</span>`;
                    },

                },
                {
                    field: 'Nhánh',
                    title: 'Nhánh',
                    autoHide: false,
                    width: 50,
                    template: function (data) {
                        return `F7`;
                    },

                },

                {
                    field: 'Đăng nhập cuối',
                    title: 'Đăng nhập cuối',
                    autoHide: false,
                    width: 140,
                    template: function (data) {
                        return `2022-01-07  17:32:16`;
                    },
                },
                {
                    field: 'Trạng thái',
                    title: 'Trạng thái',
                    autoHide: false,
                    width: 120,
                    template: function (data) {
                        return `
                            <div class="status_bar w-100">Chưa kích hoạt</div>
                        `;
                    },
                },
                {
                    field: 'action',
                    title: '',
                    autoHide: false,
                    width: 60,
                    class: ``,
                    template: function (data) {
                        return `
                            <div class="d-flex align-items-center">
                                <i class="ri-mail-fill kt-datatable__action-icon"></i>
                                <i class="ri-admin-fill kt-datatable__action-icon ms-4"></i>
                            </div>
                        `;
                    },
                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
            translate: {
                toolbar: {
                    layout: {
                        pagination: ['info', 'nav']
                    },
                }
            }
        });
    }

    initControl() {
        let that = this;
        //$('.form-select').not('.ver_footer').selectpicker();
        $(`#member_index_dropdonwSetting`).on(`shown.bs.dropdown`, e => {
            let daterangepicker2Input = new that.func.Daterangepicker2Input(`#formFilter_timeStart`, `#formFilter_timeEnd`);
        });
        $(`.dropdown-menu.ver_2 > .dropdown-header .btn-close`).off(`click`).on(`click`, e => {
            $(e.currentTarget).parents(`.dropdown-menu`).parent().find(`[data-bs-toggle="dropdown"]`).dropdown("hide");
        });
    }

    initChart(type, test) {
        if (test == true) {
            this.chartMoneyStatistics.destroy();
            this.chartMoneyStatistics = null;
            //$("#chart_money_statistics_home").remove();
            //$('.append-chart').append(`<canvas id="chart_money_statistics_home" width="400" style="margin-top: 20px;"></canvas>`)

        }
        let that = this

        $.ajax({
            url: `/home/getsChart`,
            method: 'POST',
            data: {
                type: type,

            }
        }).done(function (response) {
            var cuslabels = [];
            var dataDeposit = [];
            var dataWithdraw = [];
            if (response.data) {
                $.each(response.data.items, function (i, v) {
                    cuslabels.push(v.time_format);
                    dataDeposit.push(v.countCharge);
                    dataWithdraw.push(-(v.countWithdraw));
                })
            }
            that.chartMoneyStatistics = new Chart(document.getElementById('chart_money_statistics_home').getContext('2d'), {
                type: 'bar',
                data: {
                    labels: cuslabels,
                    datasets: [
                        {
                            label: "Khối lượng nạp vào", /*Deposit amount*/
                            backgroundColor: "#2CAE39",
                            width: `5px`,
                            categoryPercentage: 0.4,
                            data: dataDeposit
                        },
                        {
                            label: "Khối lượng rút ra", /* "Withdrawal amount",*/
                            backgroundColor: "#FF6838",
                            categoryPercentage: 0.4,
                            data: dataWithdraw
                        }
                    ]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                        },
                    },
                    plugins: {
                        legend: {
                            display: false,
                        }
                    }
                },
                plugins: [{
                    afterUpdate(chart, args, options) {
                        $(`.chartjs_legend_container`).empty();

                        var html = `
                        <div class="d-flex">
                    `;

                        chart.legend.legendItems.forEach((item, i) => {
                            let total = 0;
                            chart._sortedMetasets[i]._parsed.forEach((item2, i2) => {
                                total += item2.y;
                            });
                            if (i == 1) {
                                total = total * -1;
                            }

                            html += `
                            <div>
                                <div class="d-flex align-items-center me-md-20 me-1">
                                    <div class="legend-item" style="background: ${item.fillStyle}"></div>
                                    <span class="text_css _title_small ms-2">${item.text}</span>
                                </div>
                                <div class="text_css _contain_big" style="margin-top: 4px;">${total} USDT</div>
                            </div>
                        `;
                        });

                        html += `</div>`

                        $('.chartjs_legend_container').append(html);
                    }
                }],
            });
        }).fail(function (data) {
            toastr.error(data.responseJSON.message);
        })

        //this.chartActiveUsers = new Chart(document.getElementById('chart_ActiveUsers_home').getContext('2d'), {
        //    type: 'bar',
        //    data: {
        //        labels: ['05/1', '06/1', '07/1', '08/1', '09/1', '10/1', '11/1', '05/1', '06/1', '07/1', '08/1', '09/1', '10/1', '11/1', '05/1', '06/1', '07/1', '08/1', '09/1', '10/1', '11/1'],
        //        datasets: [
        //            {
        //                label: "Tham gia trực tiếp", /*Participate directly*/
        //                backgroundColor: "#2CAE39",
        //                width: `5px`,
        //                categoryPercentage: 0.4,
        //                data: [70000, 80000, 30000, 70000, 80000, 344, 30000, 70000, 80000, 30000, 70000, 80000, 344, 30000, 70000, 80000, 30000, 70000, 80000, 344, 30000]
        //            },
        //            {
        //                label: "Giới thiệu tham gia",/* "Introduction to join",*/
        //                backgroundColor: "#FF6838",
        //                categoryPercentage: 0.4,
        //                data: [0, 90000, 735, 0, 90000, 70000, 5345, 0, 90000, 735, 0, 90000, 70000, 5345, 0, 90000, 735, 0, 90000, 70000, 5345]
        //            }
        //        ]
        //    },
        //    options: {
        //        scales: {
        //            y: {
        //                beginAtZero: true,
        //            },
        //            x: {
        //                stacked: true,
        //            },
        //        },
        //        plugins: {
        //            legend: {
        //                display: false,
        //            }
        //        }
        //    },
        //    plugins: [{
        //        afterUpdate(chart, args, options) {
        //            console.log(chart)
        //            console.log(chart.legend)
        //            console.log(chart.legend.legendItems)
        //            $(`#legend_activeUsers_home`).empty();

        //            var html = `
        //                <div class="d-flex">
        //            `;

        //            chart.legend.legendItems.forEach((item, i) => {
        //                let total = 0;
        //                chart._sortedMetasets[i]._parsed.forEach((item2, i2) => {
        //                    total += item2.y;
        //                });

        //                html += `
        //                    <div>
        //                        <div class="d-flex align-items-center me-20">
        //                            <div class="legend-item" style="background: ${item.fillStyle}"></div>
        //                            <span class="text_css _title_small ms-2">${item.text}</span>
        //                        </div>
        //                        <div class="text_css _contain_big" style="margin-top: 4px;">${total}</div>
        //                    </div>
        //                `;
        //            });

        //            html += `</div>`

        //            $('#legend_activeUsers_home').append(html);
        //        }
        //    }],
        //});
    }
    initChartStaticalNru(type, test) {
        let that = this;
        if (test == true) {
            this.chartStaticalNru.destroy();
            this.chartStaticalNru = null;
        }
        $.ajax({
            url: `/home/getsChartStaticalNru`,
            method: 'POST',
            data: {
                type: type,
            }
        }).done(function (response) {
            var cuslabels = [];
            var value = [];
            var decimalValue = [];
            if (response.data) {
                $.each(response.data, function (i, v) {
                    cuslabels.push(v.time);
                    value.push(v.value);
                    decimalValue.push(v.decimalValue);
                })
            }
            that.chartStaticalNru = new Chart(document.getElementById('chart_statical_nru').getContext('2d'), {
                type: 'line',
                data: {
                    labels: cuslabels,
                    datasets: [
                        {

                            label: "Số lượng User đăng ký mới", /*Deposit amount*/
                            fill: true,
                            width: `5px`,
                            categoryPercentage: 0.4,
                            data: value,
                            backgroundColor: [
                                'rgba(204,0,102,0.2)',
                            ],
                            borderColor: [
                                'rgba(204,0,102,1)',
                            ],
                            borderWidth: 1
                        },

                    ]
                },
                options: {
                    scales: {
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        },
                        'samples-filler-analyser': {
                            target: 'chart-analyser'
                        }
                    },
                    interaction: {
                        intersect: false,
                    },
                    elements: {
                        line: {
                            tension: 0.2
                        }
                    },
                },
            });
        }).fail(function (data) {
            toastr.error(data.responseJSON.message);
        })
    }
    initChartStaticalUac(type, test) {
        let that = this;
        if (test == true) {
            this.chartStaticalUac.destroy();
            this.chartStaticalUac = null;
        }
        $.ajax({
            url: `/home/getsChartStaticalUac`,
            method: 'POST',
            data: {
                type: type,
            }
        }).done(function (response) {
            var cuslabels = [];
            var value = [];
            var decimalValue = [];
            if (response.data) {
                $.each(response.data, function (i, v) {
                    cuslabels.push(v.time);
                    value.push(v.value);
                    decimalValue.push(v.decimalValue);
                })
            }

            that.chartStaticalUac = new Chart(document.getElementById('chart_statical_uac').getContext('2d'), {
                type: 'line',
                data: {
                    labels: cuslabels,
                    datasets: [
                        {
                            label: "Số lượng User hoạt động", /*Deposit amount*/
                            fill: true,
                            width: `5px`,
                            categoryPercentage: 0.4,
                            data: value,
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.2)',
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)',
                            ],
                            borderWidth: 1
                        },

                    ]
                },
                options: {
                    scales: {
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        },
                        'samples-filler-analyser': {
                            target: 'chart-analyser'
                        }
                    },
                    interaction: {
                        intersect: false,
                    },
                    elements: {
                        line: {
                            tension: 0.2
                        }
                    },
                },
            });
        }).fail(function (data) {
            toastr.error(data.responseJSON.message);
        })
    }
    initChartStaticalTradeVolume(type, test) {
        let that = this;
        if (test == true) {
            this.chartStaticalTradeVolume.destroy();
            this.chartStaticalTradeVolume = null;
        }
        $.ajax({
            url: `/home/getsChartStaticalTradeVolume`,
            method: 'POST',
            data: {
                type: type,
            }
        }).done(function (response) {
            var cuslabels = [];
            var value = [];
            var decimalValue = [];
            if (response.data) {
                $.each(response.data, function (i, v) {
                    cuslabels.push(v.time);
                    value.push(v.value);
                    decimalValue.push(v.decimalValue);
                })
            }
            that.chartStaticalTradeVolume = new Chart(document.getElementById('chart_statical_trade_volume').getContext('2d'), {
                type: 'line',
                data: {
                    labels: cuslabels,
                    datasets: [
                        {
                            label: "Khối lượng giao dịch", /*Deposit amount*/
                            fill: true,
                            width: `5px`,
                            categoryPercentage: 0.4,
                            data: decimalValue,
                            backgroundColor: [
                                'rgba(0,0,204,0.2)',
                            ],
                            borderColor: [
                                'rgba(0,0,204,1)',
                            ],
                            borderWidth: 1
                        },

                    ]
                },
                options: {
                    scales: {
                        y: {
                            stacked: true
                        }
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        },
                        'samples-filler-analyser': {
                            target: 'chart-analyser'
                        }
                    },
                    interaction: {
                        intersect: false,
                    },
                    elements: {
                        line: {
                            tension: 0.2
                        }
                    },
                },
            });
        }).fail(function (data) {
            toastr.error(data.responseJSON.message);
        })
    }
 
}



$(document).ready(e => {


    let home = new Home(`.admin-home`);
});