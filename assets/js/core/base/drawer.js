"use strict";

// plugin setup
var KTDrawer = function (options) {
    var focused = document.activeElement;
    focused.classList.forEach(function (item) {
        $(`.${item}`).blur();
    });

    // Main object
    var the = this;

    // Get element object
    var $drawer;

    // Default options
    var defaultOptions = {};

    ////////////////////////////
    // ** Private Methods  ** //
    ////////////////////////////

    var Plugin = {
        /**
         * Construct
         */

        construct: function (options) {
            Plugin.init(options);

            return the;
        },

        /**
         * Handles subtoggle click toggle
         */
        init: function (options) {
            the.events = [];

            // merge default and user defined options
            the.options = KTUtil.deepExtend({}, defaultOptions, options);
        },

        open: function () {

            const { id, onRenderHeader, onRenderFooter, title, className } = the.options;

            const html = `
        <div class="Project-drawer Project-drawer--${id || Date.now()} ${className || ''}">
          <div class="Project-drawer__container">
            <div class="Project-drawer__body">
              <div class="Project-drawer__content"></div>
            </div>
          </div>
          <div class="Project-drawer__backdrop"></div>
        </div>
      `;

            $drawer = $(html);
            const $body = $drawer.find('.Project-drawer__body');
            this.block();
            /**
             * Header
             */
            if (title || onRenderHeader) {
                const $header = $(`<div class="Project-drawer__header d-flex align-items-center"></div>`);

                if ('function' === typeof onRenderHeader) {
                    onRenderHeader((content) => {
                        $header.append(content);
                        $header.insertBefore($body);
                    });
                } else if (title) {
                    $header.html(`
            <h5 class="Project-drawer__title m-0">${title}</h5>
            <button type="button" class="close ml-auto" aria-label="Close"></button>
          `);
                    $header.insertBefore($body);
                }
            }

            /**
             * Footer
             */
            if ('function' === typeof onRenderFooter) {
                const $footer = $(`<div class="Project-drawer__footer"></div>`);

                onRenderFooter((content) => {
                    $footer.append(content);
                    $footer.insertAfter($body);
                });
            }

            $drawer.appendTo('body');

            $('body').addClass('drawer-open');

            setTimeout(() => {
                $drawer.addClass('Project-drawer--open');
            }, 1);

            setTimeout(() => {
                Plugin.eventTrigger('ready');
            }, 500);

            this.initEventListeners();

        },

        initEventListeners: function () {
            if ($drawer.parent('body').hasClass('Project-bunker') == true) {
                $drawer.find('.close').on('click', function () {
                    the.close();
                });
            }
            else if ($drawer.hasClass('Project-drawer--plan') == true) {
                $drawer.find('.close').on('click', function () {
                    the.close();
                });
            }
            else {
                $drawer.find('.Project-drawer__backdrop, .Project-drawer__header .close').on('click', function () {
                    the.close();
                });
            }
            //$drawer.find('.Project-drawer__backdrop, .Project-drawer__header .close').on('click', function () {
            //  the.close();
            //});
        },

        block: function () {
            KTApp1.block($drawer.find('.Project-drawer__container'));
        },

        unblock: function () {
            KTApp1.unblock($drawer.find('.Project-drawer__container'));
        },

        setHeader: function () { },

        setBody: function (content) {
            const $content = $drawer.find('.Project-drawer__content');
            $content.append(content);
            this.unblock();
            return $content;
        },

        error: function (error) {
            if (error) {
                if (error.message != null) {
                    $.notify(error.message, { type: 'danger' });
                }
                else {
                    $.notify(error.msg, { type: 'danger' });
                }
            }
            this.close();
        },

        setFooter: function () { },

        close: function () {
            $drawer.removeClass('Project-drawer--open').find('.Project-drawer__content').empty();
            setTimeout(() => {
                const { id } = the.options;
                KTApp1.unblock($drawer.find('.Project-drawer__body'));
                $(`.Project-drawer--${id || Date.now()}`).remove();
            }, 500);

            $('body').removeClass('drawer-open');
        },

        getElement: function () {
            return $drawer;
        },

        getHeaderElement: function () {
            return $drawer.find('.Project-drawer__header');
        },

        getBodyElement: function () {
            return $drawer.find('.Project-drawer__body');
        },

        getFooterElement: function () {
            return $drawer.find('.Project-drawer__footer');
        },

        /**
             * Trigger events
             */
        eventTrigger: function (name) {
            //KTUtil.triggerCustomEvent(name);
            for (var i = 0; i < the.events.length; i++) {
                var event = the.events[i];
                if (event.name == name) {
                    if (event.one == true) {
                        if (event.fired == false) {
                            the.events[i].fired = true;
                            return event.handler.call(this, the);
                        }
                    } else {
                        return event.handler.call(this, the);
                    }
                }
            }
        },

        addEvent: function (name, handler, one) {
            the.events.push({
                name: name,
                handler: handler,
                one: one,
                fired: false
            });

            return the;
        }
    };

    //////////////////////////
    // ** Public Methods ** //
    //////////////////////////

    /**
     * Set default options 
     */

    the.setDefaults = function (options) {
        defaultOptions = options;
    };

    the.setBody = function (content) {
        return Plugin.setBody(content);
    }

    the.error = function (error) {
        return Plugin.error(error);
    }

    the.block = function () {
        return Plugin.block();
    };

    the.unblock = function () {
        return Plugin.unblock();
    };

    the.getElement = function () {
        return Plugin.getElement();
    };

    the.getHeaderElement = function () {
        return Plugin.getHeaderElement();
    };

    the.getBodyElement = function () {
        return Plugin.getBodyElement();
    };

    the.getFooterElement = function () {
        return Plugin.getFooterElement();
    };


    /**
     * Show drawer 
     */
    the.open = function () {
        return Plugin.open();
    };

    /**
     * Hide dialog
     */
    the.close = function () {
        return Plugin.close();
    };

    /**
       * Attach event
       */
    the.on = function (name, handler) {
        return Plugin.addEvent(name, handler);
    };

    /**
     * Attach event that will be fired once
     */
    the.one = function (name, handler) {
        return Plugin.addEvent(name, handler, true);
    };

    // Construct plugin
    Plugin.construct.apply(the, [options]);

    return the;
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = KTDrawer;
}