"use strict";

import jQuery from 'jquery';

var valGetParentContainer = function (element) {
    var element = $(element);

    if ($(element).closest('.form-group-sub').length > 0) {
        return $(element).closest('.form-group-sub')
    } else if ($(element).closest('.bootstrap-select').length > 0) {
        return $(element).closest('.bootstrap-select')
    } else {
        return $(element).closest('.form-group');
    }
}

jQuery.validator.setDefaults({
    errorElement: 'div', //default input error message container
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",  // validate all fields including form hidden input

    errorPlacement: function (error, element) { // render error placement for each input type
        var element = $(element);

        var group = valGetParentContainer(element);

        var help = group.find('.form-text');

        if (group.find('.valid-feedback, .invalid-feedback').length !== 0) {
            return;
        }

        element.addClass('is-invalid');
        error.addClass('invalid-feedback');

        if (help.length > 0) {
            help.before(error);
        } else {
            if (element.closest('.bootstrap-select').length > 0) {     //Bootstrap select
                element.closest('.bootstrap-select').find('.bs-placeholder').after(error);
            } else if (element.closest('.input-group').length > 0) {   //Bootstrap group
                //element.group(error);
                group.append(error);
            } else {                                                   //Checkbox & radios
                if (element.is(':checkbox')) {
                    element.closest('.kt-checkbox').find('> span').after(error);
                } else {
                    group.append(error);
                }
            }
        }
    },

    highlight: function (element) { // hightlight error inputs
        var group = valGetParentContainer(element);
        group.addClass('validate');
        group.addClass('is-invalid');
        $(element).removeClass('is-valid');
    },

    unhighlight: function (element) { // revert the change done by hightlight
        var group = valGetParentContainer(element);
        group.removeClass('validate');
        group.removeClass('is-invalid');
        $(element).removeClass('is-invalid');
    },

    success: function (label, element) {
        var group = valGetParentContainer(element);
        group.removeClass('validate');
        group.find('.invalid-feedback').remove();
    }
});

jQuery.validator.addMethod("email", function (value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value) || value == "") {
        return true;
    } else {
        return false;
    }
}, "Email không hợp lệ.");
jQuery.validator.addMethod('gmail', function (value, element) {
    return this.optional(element) || /^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/i.test(value);
}, "Không đúng định dạng của Gmail");

jQuery.validator.addMethod("phoneVN", function (value, element) {
    
    var phone = $.trim(value);
    return (/^(\+84|84|0)(3[2-9]|7[6-9]|8[1-5]|70|56|58|59|9[0-9]|86|88|89)([0-9]{7})$/.test(phone));
}, "Số điện thoại không đúng");
jQuery.validator.addMethod("phoneVNOrEmpty", function (value, element) { // ++++
    
    var phone = $.trim(value);
    return phone != "" ? (/^(84|0)(3[2-9]|7[6-9]|8[1-5]|70|56|58|59|9[0-9]|86|88|89)([0-9]{7})$/.test(phone)) : true;
}, "Số điện thoại không đúng");
jQuery.validator.addMethod("test", function (value, element) {
    
    var phone = $.trim(value);
    if (phone == undefined || phone == "") {
        return true;
    }
    return (/^([0-9])+(.|\,)+([0-9])+$/.test(phone));
  
}, "Vui lòng không nhập chữ");
jQuery.validator.addMethod("checkpoint", function (value, element) {
    
    var check = $.trim(value);
    if (check.includes(",") == false) {
        return true;
    }
    

}, "Vui thay trọng số bằng ");
jQuery.validator.addMethod("existName", function (value) {
    var name = $.trim(value);
    var ret = false;
    $.ajax({
        type: "POST",
        url: "/TreeCategories/checkname",
        async: false,
        data: { name: name },
        dataType: "json",
        success: function (error) {
            ret = error.error == 0 ? true : false
        },
        
    });
    return ret;
}, "Username exist try again");
jQuery.validator.addMethod("mytst", function (value, element) {
    var flag = true;

    $("[name*= packageUuid]").each(function (i, j) {
        $(this).parent('p').find('label.error').remove();
        $(this).parent('p').find('label.error').remove();
        if ($.trim($(this).val()) == '') {
            flag = false;

        }
    });


    return flag;


}, "aaaa");


 