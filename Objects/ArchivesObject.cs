﻿using ProjectAdmin.Utilities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Objects
{
    public class ArchivesObject
    {
        public string uuid { get; set; }

        public class Export
        {
            public class AddItem
            {
                public string uuid { get; set; }
                public string name { get; set; }
                public string inventory { get; set; }
                public string init { get; set; }
            }
            public class AddItems
            {
                public List<AddItem> items { get; set; }
            }
        }
    }
}
