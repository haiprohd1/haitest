﻿using ProjectAdmin.Utilities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Objects
{
    public class RequestHeader
    {
        public string Authorization { get; set; }
        public string ContentType { get; set; }
        public string Method { get; set; }
        public string Action { get; set; }
        public string Uuid { get; set; }

    }

    public class PaginationRequest
    {
        public int page { get; set; }
        public int limit { get; set; }
    }

    public class BaseRequest
    {
        public int page { get; set; }
        public int limit { get; set; }
        public string keyword { get; set; }
        public string tokenStr { get; set; }
    }

    public class FileInputRequest
    {
        public List<IFormFile> FileToUpload { get; set; }
    }

}
