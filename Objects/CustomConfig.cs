﻿using ProjectAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace ProjectAdmin.Objects
{
    public class CustomConfigApi
    {
        public int MainPort { get; set; } = 80;
        public string Refer { get; set; }
        public string EndPoint { get; set; }
        public string EndPoint1 { get; set; }
        public string ImageDomain { get; set; }
        public string DownloadExcel { get; set; }
        public Account Account { get; set; }
        public CMSAccount CMSAccount { get; set; }
        public CMSReport CMSReport { get; set; }
        public Dashboard Dashboard { get; set; }
        public CMSTransaction CMSTransaction { get; set; }
        public Staff Staff { get; set; }
        public Image Images { get; set; }
        public OverView OverView { get; set; }
        public Member Member { get; set; }
        public Match Match { get; set; }
        public Report Report { get; set; }
        public CMSNotificationEvent CMSNotificationEvent { get; set; }
    }
    public class CMSNotificationEvent
    {
        public string List { get; set; }
        public string Add { get; set; }
        public string Edit { get; set; }
        public string SetState { get; set; }
        public string Delete { get; set; }
    }
    public class Account
    {
        public string Login { get; set; }
        public string Logout { get; set; }
        public string RefreshToken { get; set; }
        public string Gets { get; set; }
        public string Reset { get; set; }
        public string ChangePassword { get; set; }
        public string Profile { get; set; }
        public string Sip { get; set; }
        public string AddSip { get; set; }
        public string Ami { get; set; }
        public string UpdateAmi { get; set; }
        public string Delete { get; set; }
        public string Menus { get; set; }


    }
    public class Staff
    {
        public string Gets { get; set; }
        public string Teams { get; set; }
    }
    public class OverView
    {
        public string Statical { get; set; }
        public string DepositWithdraw { get; set; }
        public string StaticalNru { get; set; }
        public string StaticalUac { get; set; }
        public string StaticalTradeVolume { get; set; }
        public string StaticalMoment { get; set; }
    }
    public class Image
    {
        public string Upsert { get; set; }
        public string UpsertStaff { get; set; }
    }    
    public class CMSAccount
    {
        public string Gets { get; set; }
        public string Detail { get; set; }
        public string ListSession { get; set; }
        public string UpdateStatus { get; set; }
        public string ListAccountInvest { get; set; }
        public string InfoGame { get; set; }
    }    
    public class CMSTransaction
    {
        public string Gets { get; set; }
        public string ListCharges { get; set; }
        public string ListWithdraw { get; set; }
        public string ListAction { get; set; }
        public string ListProduct{ get; set; }
        public string ListChargeStatus{ get; set; }
        public string ListWithdrawStatus { get; set; }
        public string WithdrawChangeStatus { get; set; }
        public string ListTransaction { get; set; }
        public string ListBet { get; set; }
        public string VerifyDeposit { get; set; }
        public string ListRequestWithdraw { get; set; }
    }         
    public class Dashboard
    {
        public string RecordChart { get; set; }
    }    
    public class CMSReport
    {
        public string Gets { get; set; }
    }
    public class Member
    {
        public string GetBranch { get; set; }
        public string GetMember { get; set; }
        public string ChangeState { get; set; }
        public string Detail { get; set; }
        public string DetailList { get; set; }
        public string Chart { get; set; }
    } 
    public class Match
    {
        public string Gets { get; set; }
        public string ListMatchUnPick { get; set; }
        public string DetailsListBet { get; set; }
        public string PickMatch { get; set; }
        public string SetMatchBet { get; set; }
        public string ListMatchUnSetBet { get; set; } 
        public string Detail { get; set; }
        public string Leagues { get; set; }
        public string EditMatchBetInterest { get; set; }
        public string SetMatchOrder { get; set; }
        public string SetMatchScore { get; set; }
        public string SetMatchStatus { get; set; }
        public string ListUserBet { get; set; }

    }
    public class Report
    {
        public string PaymentOverview { get; set; }
        public string ListPaymentDetails { get; set; }
    }
}
