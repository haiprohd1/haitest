﻿using Microsoft.AspNetCore.Mvc;
using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Controllers
{
    public class TransactionHistoryController : BaseController<TransactionHistoryController>
    {
        private readonly IHomeService _homeService;

        public TransactionHistoryController(IHomeService homeService)
        {
            _homeService = homeService;

        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult test(DashboardRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Match.Gets;
            var obj = _homeService.WithDraw(header, request);
            if (obj?.error != null && (EnumError)obj.error.code == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult Detail()
        {
            return View();
        }
    }
}
