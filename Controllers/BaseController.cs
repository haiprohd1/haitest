﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.DependencyInjection;
using ProjectAdmin.Objects;
using ProjectAdmin.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjectAdmin.Models;
using ProjectAdmin.CustomAttributes;
using System.Text;


namespace ProjectAdmin.Controllers
{
    [Authorize]
    public abstract class BaseController<T> : Controller where T : BaseController<T>
    {
        private IOptions<CustomConfigApi> _customConfig;
        private ILogger<T> _logger;

        protected ILogger<T> Logger => _logger ?? (_logger = HttpContext.RequestServices.GetService<ILogger<T>>());
        protected IOptions<CustomConfigApi> CustomConfig => _customConfig ?? (_customConfig = HttpContext.RequestServices.GetService<IOptions<CustomConfigApi>>());

        protected ClaimsIdentity ClaimsIdentity => User.Identity as ClaimsIdentity;

        protected CustomConfigApi CustomConfigApi => CustomConfig.Value;
        protected RequestHeader Header =>
            new RequestHeader
            {
                Authorization = ProjectToken,
                ContentType = "application/json",
                Method = "POST",
            };


        protected string ProjectToken => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(Constants.TOKEN).Value : string.Empty;

        protected string ProjectRefreshToken => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(Constants.REFRESH_TOKEN).Value : string.Empty;

        public string Username => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(ClaimTypes.Name).Value : string.Empty;

        public string SubHeaderData => Constants.SubHeaderData;
        

    }
}