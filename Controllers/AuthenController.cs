﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ProjectAdmin.Objects;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProjectAdmin.Models;
using ProjectAdmin.CustomAttributes;
using ProjectAdmin.Interfaces;
using ProjectAdmin.Utilities;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;

namespace ProjectAdmin.Controllers
{
    [AllowAnonymous]
    public class AuthenController : BaseController<AuthenController>
    {

        private readonly IAuthenService _authenService;
        private readonly IAccountService _accountService;

        public AuthenController(IAuthenService authenService, IAccountService accountService)
        {
            _authenService = authenService;
            _accountService = accountService;
        }

        //[Route("/login")]
        public IActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }
        public IActionResult ForgotPassword()
        {
            var model = new LoginModel();
            return View(model);
        }
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture ?? "")),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
        public IActionResult Gets()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {

            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Account.Login;
                var request = new LoginRequest
                {
                    passStr = model.passStr,
                    userStr = model.userStr,
                    transferKeyStr = "web"
                };

                var obj = _authenService.Login(header, request);
                if (obj != null && obj.data != null && obj.error.code == 0)
                {
                    var claims = new List<Claim>
                    {
                        //new Claim(ClaimTypes.Name, obj.data.staff.account.username),
                        new Claim(Constants.FULL_NAME, obj.data.user.username),
                        //new Claim(Constants.USER_ROLE, obj.data.staff.account.role.name),
                        //new Claim(Constants.USER_POSITION, obj.data.staff.companyTree.osType),
                        new Claim(Constants.TOKEN, obj.data.token.key),
                        new Claim(Constants.TTL, obj.data.token.timeDestroy.ToString()),
                        new Claim(Constants.USER_ROLE, obj.data.user.role_id.ToString()),
                        //new Claim(Constants.USER_AVATAR,CustomConfigApi.ImageDomain + obj.data.staff.avatarPath),
                        //new Claim(Constants.USER_MENUS, Get_Menus(obj.data.token).Count > 0 ? JsonConvert.SerializeObject(Get_Menus(obj.data.token)) : ""),
                    };

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    await HttpContext.SignInAsync(
                        scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                        principal: new ClaimsPrincipal(claimsIdentity),
                        properties: new AuthenticationProperties
                        {
                            IsPersistent = true,
                        }).ConfigureAwait(false);
                    if (obj.data.user.role_id == 4)
                    {
                        return RedirectToAction("Index", "ProfitReport");
                    }
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    

                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                }
                else
                {
                    if (model.userStr == null)
                    {
                        var username = model.language == null ? "Bạn chưa nhập tên đăng nhập" : "This is a required field";
                        ModelState.AddModelError("userStr", username);
                    }
                    if (model.passStr == null)
                    {
                        var passStr = model.language == null ? "Bạn chưa nhập mật khẩu" : "This is a required field";
                        ModelState.AddModelError("passStr", passStr);
                    }
                    else if (model.passStr.Length <= 6)
                    {
                        var passStrLength = model.language == null ? "Mật khẩu phải từ 6 ký tự trở lên" : "Password must be 6 or more characters";
                        ModelState.AddModelError("passStr", passStrLength);
                    }

                    var html = model.language == null ? "Thông tin đăng nhập không chính xác" : "Login information is incorrect!";
                    ModelState.AddModelError("", html);
                }
            }
            return View(model);
        }


        public async Task<IActionResult> Logout()
        {
            var logout = await LogoutAsync();
            if (logout)
            {
                return RedirectToAction("login", "authen");
            }
            else
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return RedirectToAction("login", "authen");
            }
            /*
            var header = Header;

            header.Action = CustomConfigApi.Account.Logout;            

            var obj = _authenService.Logout(header);
            if (obj != null && obj.error == 0)
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return RedirectToAction("login", "authen");
            }
            return RedirectToAction("error","home");
            */
        }

        [HttpPost]
        public async Task<IActionResult> LogoutAjax()
        {
            var logout = await LogoutAsync();
            if (logout)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
            /*
            var header = Header;

            header.Action = CustomConfigApi.Account.Logout;            

            var obj = _authenService.Logout(header);
            if (obj != null && obj.error == 0)
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return RedirectToAction("login", "authen");
            }
            return RedirectToAction("error","home");
            */
        }

        private async Task<bool> LogoutAsync()
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.Logout;
            var request = new LogoutRequest
            {
                tokenStr = header.Authorization,

            };
            var obj = _authenService.Logout(header, request);
            if (obj != null && obj.error.code == 0)
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return true;
            }
            return false;
        }

        private async Task<bool> RefreshTokenAsync()
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.RefreshToken;

            var request = new RefreshTokenRequest
            {
                refresh_token = ProjectRefreshToken
            };

            var obj = _authenService.RefreshToken(header, request);

            if (obj != null && obj.error.code == 0)
            {
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, Username),
                        new Claim(Constants.TOKEN, obj.data.token.key),
                        new Claim(Constants.REFRESH_TOKEN, ProjectRefreshToken),
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: new ClaimsPrincipal(claimsIdentity),
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = true,
                    }).ConfigureAwait(true);
                return true;
            }
            else
            {
                var logout = LogoutAsync();

                return false;
            }

        }

        [Route("authen/refresh")]
        public IActionResult RefreshToken(string currentUrl)
        {
            var refreshToken = RefreshTokenAsync();
            if (refreshToken.Result)
            {
                if (string.IsNullOrEmpty(currentUrl))
                {
                    return RedirectToPage("/home");
                }
                else
                {
                    return RedirectToPage(currentUrl);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"MainPort: {CustomConfigApi.MainPort}");
                return RedirectToPage("~/authen/login");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Refresh_Token()
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.RefreshToken;

            var request = new RefreshTokenRequest
            {
                refresh_token = ProjectRefreshToken
            };

            var obj = _authenService.RefreshToken(header, request);

            if (obj != null && obj.error.code == 0)
            {
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, Username),
                        new Claim(Constants.TOKEN, obj.data.token.key),
                        new Claim(Constants.REFRESH_TOKEN, ProjectRefreshToken),
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: new ClaimsPrincipal(claimsIdentity),
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = true,
                    }).ConfigureAwait(true);
            }
            return Ok(obj);
        }

        private List<string> Get_Menus(string Authorization)
        {
            var entries = new List<string>();

            var header = Header;
            header.Action = CustomConfigApi.Account.Menus;
            header.Authorization = Authorization;

            var obj = _accountService.Menus(header);

            if (obj.data != null && (EnumError)obj.error.code == EnumError.Success)
            {
                entries = obj.data.items;

                return entries;
            }

            return entries;
        }
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ChangPassword(ChangePass request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.ChangePassword;
            var obj = _authenService.ChangePassword(header, request);
            if (obj != null && (EnumError)obj.error.code == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
    }
}