﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectAdmin.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using ProjectAdmin.Interfaces;
using ProjectAdmin.Utilities;

namespace ProjectAdmin.Controllers
{
    
    public class HomeController : BaseController<HomeController>
    {
        private readonly ILogger _logger;
        private readonly IHomeService _homeService;
       
        public HomeController(IHomeService homeService,ILogger<HomeController> logger)
        {
            _logger = logger;
            _homeService = homeService;
          
        }
        //public IActionResult Index()
        //{
        //    var subHeader = new SubHeaderModel
        //    {

        //        //Title = "Quản lý ứng dụng",
        //        //SubTitleSmal = "<span class='a'>Ứng dụng được phát triển bởi</span>"
        //    };
        //    ViewData[SubHeaderData] = subHeader;
        //    var request = new DashboardRequest();
        //    var header = Header;
        //    header.Action = CustomConfigApi.OverView.Statical;
        //    var obj = _homeService.Dashboard(header, request);
        //    return View(obj.data);
        //}
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {

                //Title = "Quản lý ứng dụng",
                //SubTitleSmal = "<span class='a'>Ứng dụng được phát triển bởi</span>"
            };
            ViewData[SubHeaderData] = subHeader;
            var request = new DashboardRequest();
            var header = Header;
            header.Action = CustomConfigApi.OverView.StaticalMoment;
            var obj = _homeService.Dashboard(header, request);
            return View(obj?.data);
        }
        public IActionResult getsChart(DashboardRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.OverView.DepositWithdraw;
            var obj = _homeService.WithDraw(header, request);
            if (obj != null && obj?.error != null && (EnumError)obj.error.code == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult getsChartStaticalNru(DashboardRequest request)
        {
            var header = Header;
            header.Action = $"{ CustomConfigApi.OverView.StaticalNru}/?type={request.type}";
            
            var obj = _homeService.Statical(header, request);
            if (obj != null && obj?.error != null && (EnumError)obj.error.code == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj.error);
        }
        public IActionResult getsChartStaticalUac(DashboardRequest request)
        {
            var header = Header;
            header.Action = $"{ CustomConfigApi.OverView.StaticalUac}/?type={request.type}";
            var obj = _homeService.Statical(header, request);
            if (obj != null && obj?.error != null)
            {
                return Ok(obj);
            }
            return NotFound(obj.error);
        }
        public IActionResult getsChartStaticalTradeVolume(DashboardRequest request)
        {
            var header = Header;
            header.Action = $"{ CustomConfigApi.OverView.StaticalTradeVolume}/?type={request.type}";
            var obj = _homeService.Statical(header, request);
            if (obj != null && obj?.error != null && (EnumError)obj.error.code == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        //public IActionResult getsRecordChart(DashboardRequest request)
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Dashboard.RecordChart;
        //    var obj = _homeService.Dashboard(header, request);
        //    if (obj != null && obj?.error != null && (EnumError)obj.error.code == EnumError.Success)
        //    {
        //        return Ok(obj);
        //    }
        //    return NotFound(obj);
        //}

        //public IActionResult Member()
        //{
        //    return View();
        //}

        public IActionResult Station()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult AlertConfig()
        {
            return View();
        }
        [Route("/home/applications-member/{id?}")]
        public IActionResult ApplicationsMember()
        {
            var subHeader = new SubHeaderModel
            {
                Title = "Ứng dụng Project Account",
            };
            ViewData[SubHeaderData] = subHeader;
            return View();
        }
    }
}
