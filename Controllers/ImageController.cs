﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ProjectAdmin.Controllers;
using ProjectAdmin.Models;
using ProjectAdmin.Utilities;
using ProjectAdmin.Interfaces;
using ImageMagick;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace DMMS.Controllers
{
    [AllowAnonymous]
    public class ImageController : BaseController<ImageController>
    {
        private readonly IImageService _imageService;
        public ImageController(IImageService image)
        {
            _imageService = image;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Create()

        {
            return View();
        }
        [Route("/images/UploadAva")]
        [HttpPost]
        public IActionResult UploadAva()
        {
            var header = Header;
            header.Action = CustomConfigApi.Images.Upsert;
            header.ContentType = HttpContext.Request.ContentType;
            var files = HttpContext.Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                var input = new List<FileUpload>();
                foreach (var file in files)
                {
                    var postFileName = file.FileName;
                    var stream = file.OpenReadStream();
                    //var img = Image.FromStream(stream);
                    //var scale = ImageResize.Scale(img,500,500);
                    //ImageConverter _imageConverter = new ImageConverter();
                    //byte[] xByte = (byte[])_imageConverter.ConvertTo(scale, typeof(byte[]));
                    //stream = new MemoryStream(xByte);
                    MagickImage image = new MagickImage(stream);
                    image.Format = image.Format; // Get or Set the format of the image.
                    image.Resize(500, 500); // fit the image into the requested width and height. 
                    image.Quality = 10; // This is the Compression level.
                    byte[] xByte = image.ToByteArray();


                    stream = new MemoryStream(xByte);

                    var fileBinary = new byte[stream.Length];
                    stream.Read(fileBinary, 0, fileBinary.Length);


                    input.Add(new FileUpload
                    {
                        ContentType = file.ContentType,
                        FileName = file.FileName,
                        FileBinary = fileBinary,
                        Name = file.Name,
                    });


                }

                var obj = _imageService.PostFile(header, input);
                if (obj != null && (EnumError)obj.error.code == EnumError.Success && obj.data != null)
                {
                    return Ok(obj);
                }

            }

            return NotFound();

        }
        [HttpPost("/images/upload")]
        public IActionResult Upload()
        {
            var header = Header;
            header.Action = CustomConfigApi.Images.Upsert;
            header.ContentType = HttpContext.Request.ContentType;
            var files = HttpContext.Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                var input = new List<FileUpload>();
                foreach (var file in files)
                {
                    var postFileName = file.FileName;
                    var stream = file.OpenReadStream();
                    var fileBinary = new byte[stream.Length];
                    stream.Read(fileBinary, 0, fileBinary.Length);
                    input.Add(new FileUpload
                    {
                        ContentType = file.ContentType,
                        FileName = file.FileName,
                        FileBinary = fileBinary,
                        Name = file.Name,
                    });

                }

                var obj = _imageService.PostFile(header, input);
                if (obj != null && (EnumError)obj.error.code == EnumError.Success && obj.data != null)
                {
                    //return Ok(obj.data.items.Select(i => new
                    //{
                    //    id = i.id,
                    //    path = i.path,
                    //    uuid = i.uuid
                    //}));
                }

            }

            return NotFound();

        }



    }
}