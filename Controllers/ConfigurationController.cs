﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Controllers
{
    public class ConfigurationController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ChangeLanguage()
        {
            return View();
        }
        public IActionResult ChangeDate()
        {
            return View();
        }
        public IActionResult ChangeTimeZone()
        {
            return View();
        }

    }
}
