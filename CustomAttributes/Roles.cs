﻿using System;
namespace ProjectAdmin.CustomAttributes
{
    public static class Roles
    {
        public const string DIRECTOR = "DIRECTOR";
        public const string SUPERVISOR = "SUPERVISOR";
        public const string ANALYST = "ANALYST";
    }
}
