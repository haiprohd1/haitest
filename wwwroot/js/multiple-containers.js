﻿"use strict";

// Class definition
var KTDraggableMultiple = function() {
    // Private functions
    var exampleMultiple = function() {
        var containers = document.querySelectorAll('.draggable-zone');

        if (containers.length === 0) {
            return false;
        }

        var swappable = new Sortable.default(containers, {
            draggable: '.draggable',
            handle: '.draggable .draggable-handle',
            mirror: {
                //appendTo: selector,
                appendTo: 'body',
                constrainDimensions: true
            },
            plugins: [Plugins.ResizeMirror],
        });
        //swappable.on('drag:start', (evt) => {
        //   /* alert(1)*/
        //});
        //swappable.on('sortable:sort', (evt) => {
        //   /* alert(2)*/
        //});

        swappable.on('sortable:sorted', (evt) => {
            if ($(evt.data.oldContainer).hasClass('table-old') == true && $(evt.data.newContainer).hasClass('table-new') == true) {
                debugger
                /* $(evt.data.dragEvent.data.originalSource).empty();*/
                if ($(evt.data.dragEvent.data.originalSource).find('.draggable-handle').length > 0) {
                    $(evt.data.dragEvent.data.originalSource).find('.draggable-handle').addClass('border-task-day');
                    $(evt.data.dragEvent.data.originalSource).find('.draggable-handle').removeClass('draggable-handle');
                }
                
              
                $(evt.data.dragEvent.data.originalSource).append(`
                   
                       
                       <td>11/06/2020 <span class="main-text"><i class="ri-calendar-check-line main-text" style="font-size:1.5rem"></i></span></td>
                          <td style="cursor:pointer;position:relative" class="add-staff-task">
                            <div class="symbol-group symbol-hover mb-3" style="margin-bottom:0px !important">
                            </div>
                          </td>
                       <td>
                            <i class="ri-close-fill" style="font-size:1.5rem"></i>
                        </td>

                    `);
              
               
            }
            
        });
       
    }

    return {
        // Public Functions
        init: function() {
            exampleMultiple();
        }
    };
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTDraggableMultiple.init();
});
