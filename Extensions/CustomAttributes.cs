﻿using ProjectAdmin.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProjectAdmin.Extensions
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(params string[] claim) : base(typeof(AuthorizeFilter))
        {
            Arguments = new object[] { claim };
        }
    }

    public class AuthorizeFilter : IAuthorizationFilter
    {
        readonly string[] _claim;

        public AuthorizeFilter(params string[] claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            var claimsIndentity = context.HttpContext.User.Identity as ClaimsIdentity;

            if (IsAuthenticated)
            {
                bool flagClaim = false;
                foreach (var item in _claim)
                {
                    if (context.HttpContext.User.HasClaim(item, item))
                        flagClaim = true;
                }
                if (!flagClaim)
                    context.Result = new RedirectResult("~/home/error");
            }
            else
            {
                context.Result = new RedirectResult("~/authen/login");
            }
            return;
        }
    }

    [AttributeUsage(AttributeTargets.All)]
    public class CustomController : ActionFilterAttribute
    {
        private string name;
        private string parentName;
        private string route;
        private string method;
        private bool isPage;
        private string parentController;
        private string actionName;
        private bool isShow;
        private EnumAction enumAction;
        private string icon;
        private string controllerName;

        /// <summary>
        /// Custom attribute cho controller
        /// </summary>
        /// <param name="parentName">Tên hiển thị của controller cha (Dể rỗng thì là cha)</param>
        /// <param name="parentController">Tên controller cha </param>
        /// <param name="controllerName">Tên controller</param>
        /// <param name="name">Tên hiển thị cua action</param>
        /// <param name="actionName">Tên action (tên function)</param>
        /// <param name="method">Method cua action (để rỗng trong trường hợp là page)</param>
        /// <param name="route">Route cua action</param>
        /// <param name="isPage">True: là page (sẽ hiển thị ở menu); False: là action (api dùng call ajax)</param>
        /// <param name="enumAction">Loại action: View, Edit, Add, Update, Delete, Import, Export</param>
        /// <param name="isShow">True: có hiển thị trên menu; False: không hiển thị trên menu</param>
        /// <param name="icon">Icon cua menu</param>

        public CustomController(string parentName, string parentController, string controllerName, string name, string actionName, string method, string route, bool isPage,
            EnumAction enumAction = EnumAction.View, bool isShow = true, string icon = "")
        {
            this.name = name;
            this.parentName = parentName;
            this.method = method;
            this.actionName = actionName;
            this.route = route;
            this.isPage = isPage;
            this.parentController = parentController;
            this.isShow = isShow;
            this.enumAction = enumAction;
            this.icon = icon;
            this.controllerName = controllerName;
        }

        public virtual string Icon
        {
            get
            {
                return icon;
            }
        }

        public virtual string ParentName
        {
            get
            {
                return parentName;
            }
        }

        public virtual string ActionName
        {
            get
            {
                return actionName;
            }
        }

        public virtual string ControllerName
        {
            get
            {
                return controllerName;
            }
        }

        public virtual EnumAction EnumAction
        {
            get
            {
                return enumAction;
            }
        }

        public virtual string ParentController
        {
            get
            {
                return parentController;
            }
        }

        public virtual string Name
        {
            get
            {
                return name;
            }
        }
        public virtual string Route
        {
            get
            {
                return route;
            }
        }
        public virtual string Method
        {
            get
            {
                return method;
            }
        }


        public virtual bool Parent
        {
            get
            {
                return string.IsNullOrEmpty(parentName) ? true : false;
            }
        }

        public virtual bool IsPage
        {
            get
            {
                return isPage;
            }
        }
        public virtual bool IsShow
        {
            get
            {
                return isShow;
            }
        }
    }
}
