﻿using ProjectAdmin.Utilities;
using System.Threading.Tasks;
using ProjectAdmin.Models;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.ViewComponents
{
    public class SubHeader : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new SubHeaderModel();
            if (ViewData[Constants.SubHeaderData] != null)
            {
                model = (SubHeaderModel)ViewData[Constants.SubHeaderData];
            }
            else
            {
                model = null;
            }
            return View(model);
        }
    }
}