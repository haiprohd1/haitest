﻿using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Services
{
    public class AuthenService : IAuthenService
    {
        private readonly IRestApi<ResponseData<LoginDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<LoginDataResponse>> _restApi1;

        public AuthenService(IRestApi<ResponseData<LoginDataResponse>> restApi, IRestApi<ResponseData<LoginDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }

        public ResponseData<LoginDataResponse> ChangePassword(RequestHeader header, object request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LoginDataResponse> ChangePassword(RequestHeader header, ChangePass request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<LoginDataResponse> Login(RequestHeader header, LoginRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<LoginDataResponse> Logout(RequestHeader header, LogoutRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        
        public ResponseData<LoginDataResponse> RefreshToken(RequestHeader header, RefreshTokenRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

    }
}
