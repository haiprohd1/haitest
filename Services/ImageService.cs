﻿using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using ProjectAdmin.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Services
{
    public class ImageService : IImageService
    {
        private readonly IRestApi<ResponseData<ImageDataResponse>> _restApi;
        private readonly IRestApi<ResponseImageData> _restApi1;
        public ImageService(IRestApi<ResponseData<ImageDataResponse>> restApi, IRestApi<ResponseImageData> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }
        public ResponseData<ImageDataResponse> Add(RequestHeader header, ImageAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ImageDataResponse> Add(RequestHeader header, string input)
        {
            return _restApi.CallApi(header, input);
        }

        public ResponseImageData PostFile(RequestHeader header, List<FileUpload> input)
        {
            return _restApi1.PostFile(header, input);
        }
    }
}
