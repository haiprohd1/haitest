﻿using Newtonsoft.Json;
using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using ProjectAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Services
{
    public class CMSAccountService : ICMSAccountService
    {
        private readonly IRestApi<ResponseData<CMSAccountDataResponse>> _restApi;   
        private readonly IRestApi<ResponseData<ListSessionDataResponse>> _restApi2;
        private readonly IRestApi<ResponseData<ListAccountInvestDataResponse>> _restApi3;

        public CMSAccountService(IRestApi<ResponseData<CMSAccountDataResponse>> restApi,
            IRestApi<ResponseData<ListSessionDataResponse>> restApi2, IRestApi<ResponseData<ListAccountInvestDataResponse>> restApi3)
        {
            _restApi = restApi;
            _restApi2 = restApi2;
            _restApi3 = restApi3;
        }

        public ResponseData<CMSAccountDataResponse> Detail(RequestHeader header, DetailCMSAccountRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CMSAccountDataResponse> Gets(RequestHeader header, ListCMSAccountRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj?.error != null && obj.error.code == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ListSessionDataResponse> GetsListSession(RequestHeader header, ListSessionRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj?.error != null && obj.error.code == 0)
            {
                if (obj?.data?.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<CMSAccountDataResponse> InfoGame(RequestHeader header, ListAccountInvestRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ListAccountInvestDataResponse> ListAccountInvest(RequestHeader header, ListAccountInvestRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi3.CallApi(header, inputJson);
            if (obj != null && obj.error.code == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<CMSAccountDataResponse> UpdateStatus(RequestHeader header, UpdateStatusRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
    }
}
