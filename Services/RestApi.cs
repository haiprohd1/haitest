﻿using ChromeLogger;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using ProjectAdmin.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using ProjectAdmin.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;

namespace ProjectAdmin.Services
{
    public class RestApi<T> : IRestApi<T>
    {
        private readonly IOptions<CustomConfigApi> _customConfig;
        private readonly IChromeLogger _chromeLogger;
        private IHttpContextAccessor _httpContextAccessor { get; set; }
        public RestApi(IOptions<CustomConfigApi> customConfig, IChromeLogger chromeLogger, IHttpContextAccessor httpContextAccessor)
        {
            _customConfig = customConfig;
            _chromeLogger = chromeLogger;
            _httpContextAccessor = httpContextAccessor;
        }

        private string EndPoint => _customConfig.Value.EndPoint;
        private string EndPoint1 => _customConfig.Value.EndPoint1;

        public T CallApi(RequestHeader header, string input)
        {
            try
            {
                var restApi = string.Format("{0}/{1}", EndPoint, header.Action);
                if (input.IndexOf("tokenStr") > 1)
                {
                    input = input.Replace($"tokenStr\":null", String.Format("tokenStr\":\"{0}\"", header.Authorization));
                }
                var method = Helpers.ParseEnum<Method>(header.Method);
                var client = new RestClient(restApi);
                var request = new RestRequest(method);
                _chromeLogger.Log("ENPOINT: " + restApi);
                _chromeLogger.Log("Request: " + input);
                Console.WriteLine("+++++++++++++++++++++++++++++++");
                Console.WriteLine("ENPOINT: " + restApi);
                Console.WriteLine("Request: " + input);
                if (!string.IsNullOrEmpty(header.Authorization))
                {
                    request.AddHeader("Authorization", "Bearer " + header.Authorization);
                }
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", string.IsNullOrEmpty(header.ContentType) ? "application/json" : header.ContentType);
                if (!string.IsNullOrEmpty(input))
                {
                    request.AddParameter("application/json", input, ParameterType.RequestBody);
                }
                if (!string.IsNullOrEmpty(header.Uuid))
                {
                    request.AddQueryParameter("Uuid", header.Uuid);
                }
                
                IRestResponse response = client.Execute(request);
                var check = JsonConvert.DeserializeObject<Error>(response.Content);
                if (check?.code == -2 || check?.code == -3)
                {
                    RefreshAsync();
                    return default(T);
                }
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, typeof(T).FullName, ex);
                //return default(T);
            }

        }
        private async void RefreshAsync()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync().ConfigureAwait(false);
        }
        public T PostFile(RequestHeader header, List<FileUpload> files)
        {
            try
            {
                var restApi = string.Format("{0}/{1}", EndPoint, header.Action);
                var method = Helpers.ParseEnum<Method>("PUT");
                var client = new RestClient(restApi);
                var request = new RestRequest(method);
                request.AlwaysMultipartFormData = true;
                if (!string.IsNullOrEmpty(header.Authorization))
                {
                    request.AddHeader("tokenStr", header.Authorization);
                }
                request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Content-Type", string.IsNullOrEmpty(header.ContentType) ? "application/json" : header.ContentType);
                if (files != null && files.Count > 0)
                {
                    for (var i = 0; i < files.Count; i++)
                    {
                        request.AddFileBytes(string.Format(files[i].FileNameFormat, i), files[i].FileBinary, files[i].FileName, files[i].ContentType);
                        //request.AddFile(item.Name, item.FileBinary, item.FileName, item.ContentType);                        
                    }
                }

                IRestResponse response = client.Execute(request);

                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, typeof(T).FullName, ex);
                //return default(T);
            }

        }
    }
}
