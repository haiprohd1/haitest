﻿using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRestApi<ResponseData<AccountDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<MenusStringDataResponse>> _restApi1;
        private readonly IRestApi<ResponseData<AccountDataResponse>> _restApi2;

        public AccountService(IRestApi<ResponseData<AccountDataResponse>> restApi, IRestApi<ResponseData<MenusStringDataResponse>> restApi1, IRestApi<ResponseData<AccountDataResponse>> restApi2)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
            _restApi2 = restApi2;
        }

        public ResponseData<AccountDataResponse> ChangePassword(RequestHeader header, object request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> Gets(RequestHeader header, AccountSearchRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> Profile(RequestHeader header)
        {
            return _restApi2.CallApi(header, null);
        }

        public ResponseData<MenusStringDataResponse> Menus(RequestHeader header)
        {
            return _restApi1.CallApi(header, null);
        }

        public ResponseData<AccountDataResponse> ProfileSip(RequestHeader header, ProfileSip request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> ResetPassword(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
