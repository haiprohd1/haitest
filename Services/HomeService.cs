﻿using Newtonsoft.Json;
using ProjectAdmin.Interfaces;
using ProjectAdmin.Models;
using ProjectAdmin.Objects;
using ProjectAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectAdmin.Services
{
    public class HomeService : IHomeService
    {
        private readonly IRestApi<ResponseData<DashboardDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<DashboardListDataResponse>> _restApi1;
        private readonly IRestApi<ResponseDataList<DashboardListDataResponse>> _restApi2;
        public HomeService(IRestApi<ResponseData<DashboardDataResponse>> restApi, IRestApi<ResponseData<DashboardListDataResponse>> restApi1, IRestApi<ResponseDataList<DashboardListDataResponse>> restApi2)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
            _restApi2 = restApi2;
        }
        public ResponseData<DashboardDataResponse> Dashboard(RequestHeader header, DashboardRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseDataList<DashboardListDataResponse> Statical(RequestHeader header, DashboardRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi2.CallApi(header, inputJson);
        }

        public ResponseData<DashboardListDataResponse> WithDraw(RequestHeader header, DashboardRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj?.error != null && obj.error.code == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }
    }
}
