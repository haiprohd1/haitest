﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ProjectAdmin.Utilities
{
    public class Constants
    {
        public const string SubHeaderData = "SubHeader";
        /// <summary>
        /// Minute after timeout
        /// </summary>
        public const int TTL_DELTA = 1;
        public const bool CHECK_DISPLAY_CONSOLE = true;

        public const string TOKEN = "Token";
        public const string REFRESH_TOKEN = "Refresh_Token";
        public const string TTL = "TTL";
        public const string IMAGE_UPLOAD_FORMAT = "------{0}\r\nContent-Disposition: form-data; name=\"images[{1}]\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n\r\n";
        public const string USER_AVATAR = "User_Avatar";
        public const string USER_INFO = "User_Info";
        public const string FULL_NAME = "Full_Name";
        public const string USER_ROLE = "User_Team_Name";
        public const string USER_POSITION = "User_Positon";
        //public const string USER_MENUS = "User_Menus";
        public static readonly List<string> ListConstants = new List<string> {  FULL_NAME,TOKEN, TTL, USER_ROLE };

        #region Message
        public const string MESSAGE_ERROR = "Có lỗi xảy ra, vui lòng kiểm tra lại thông tin!";
        #endregion
    }

    public static class Icon
    {
        public const string Dashboard = "flaticon2-architecture-and-city";
        public const string Account = "flaticon2-user-1";

    }

    public static class MenuTitle
    {
        public const string Dashboard = "Tổng quát";
        public const string Account = "Tài khoản";

    }

    public static class SubMenuTitle
    {
        public const string AccountInfo = "Thông tin tài khoản";

    }

    public enum EnumError
    {
        Error = 1,
        Success = 0
    }
    public enum EnumSex
    {
        [Description("Nam1")]
        Nam = 1,
        [Description("Nữ")]
        Nữ = 2,
        [Description("Khác")]
        Khác = 3
    }
    public enum EnumAction
    {
        View,
        Add,
        Edit,
        Delete,
        Import,
        Export
    }
    public static class ErrorOsType
    {
        public static string GetString(string me)
        {
            switch (me)
            {
                case "root":
                    return "Công ty";
                case "agency":
                    return "Chi nhánh";
                case "team":
                    return "Đội nhóm";
                case "group":
                    return "Phòng ban";
                case "staff":
                    return "Nhân viên";
                case "agency_staff":
                    return "Quản lý chi nhánh";
                case "group_staff":
                    return "Quản lý phòng ban";
                case "team_staff":
                    return "Quản lý đội nhóm";
                case "master":
                    return "Quản trị viên";
                case "leader":
                    return "Lãnh đạo";
                default:
                    return "Không xác định";
            }
        }
    }

}
